/*
 * File that includes helper functions that all RIP simulations make use of. They are 
 * defined here in the header file for ease of implementation so that a separate .c 
 * file is not required. All shared functionality is defined in this file. 
 */

#ifndef RIP_SIM_HELPER
#define RIP_SIM_HELPER

#define PRINT_DEBUG (false)

using namespace ns3;

// Bring a node down by closing all of its interfaces
static inline void NodeDown(Ptr<Node> node, uint32_t *interfaces, uint32_t numInterfaces)
{
	for (uint32_t i = 0; i < numInterfaces; i++)
	{
		node->GetObject<Ipv4> ()->SetDown (interfaces[i]);
	}
 }

// Bring a node up by opening all of its interfaces
static inline void NodeUp (Ptr<Node> node, uint32_t *interfaces, uint32_t numInterfaces)
{
	for (uint32_t i = 0; i < numInterfaces; i++)
	{
		node->GetObject<Ipv4> ()->SetUp (interfaces[i]);
	}
 }

// Bring a link down from nodeA's interfaceA and nodeB's interfaceB.
static inline void TearDownLink (Ptr<Node> nodeA, Ptr<Node> nodeB, uint32_t interfaceA, uint32_t interfaceB)
{
  nodeA->GetObject<Ipv4> ()->SetDown (interfaceA);
  nodeB->GetObject<Ipv4> ()->SetDown (interfaceB);
}

// Bring up a link from nodeA's interfaceA and nodeB's interfaceB.
static inline void BringUpLink (Ptr<Node> nodeA, Ptr<Node> nodeB, uint32_t interfaceA, uint32_t interfaceB)
{
  nodeA->GetObject<Ipv4> ()->SetUp (interfaceA);
  nodeB->GetObject<Ipv4> ()->SetUp (interfaceB);
}

// Helper function to print out the routing tables for all RIP nodes in the network. 
static inline void PrintRouteTables (double timeToPrint, Ptr<Node> *routerList, uint8_t numRouters, Ptr<OutputStreamWrapper> routingStream)
{
	RipHelper routingHelper;
	if (timeToPrint == 0) return;
	
	for(uint8_t i = 0; i < numRouters; i++)
	{
		routingHelper.PrintRoutingTableAt (Seconds (timeToPrint), routerList[i], routingStream);
	}
}

// Helper function to print out the distance tables for all RIP nodes in the network. 
static inline void PrintDistanceTables (double timeToPrint, NodeContainer nodesToCheck, Ptr<OutputStreamWrapper> routingStream)
{
	if (timeToPrint == 0) return;
	
	Simulator::Schedule (Seconds(timeToPrint), &PrintDistanceTableAt, nodesToCheck, routingStream);
}

// Helper function to print out the distance tables for all RIP nodes in the network. 
static inline void UseDistanceTables (double time, NodeContainer nodesToCheck, bool use)
{
	Simulator::Schedule (Seconds(time), &SelectDT, nodesToCheck, use);
}

// Create a ping application that will send traffic to pingDestination from pingSrc. 
static inline void PingSetup(char* pingDestination, Ptr<Node> pingSrc, int packetSize, Time interPacketInterval, bool showPings, Time pingStartTime, Time pingEndTime)
{
	V4PingHelper ping (pingDestination);

	ping.SetAttribute ("Interval", TimeValue (interPacketInterval));
	ping.SetAttribute ("Size", UintegerValue (packetSize));
	if (showPings)
	{
		ping.SetAttribute ("Verbose", BooleanValue (true));
	}
	ApplicationContainer apps = ping.Install (pingSrc);
	apps.Start (pingStartTime);
	apps.Stop (pingEndTime);
}

// Print out when convergence is reached in a RIP node. By default convergence is considered reached
// if the routing table in a node has not changed for 30 seconds. This is the 'quiet time.'
static inline void CheckConvergence(NodeContainer nodesToCheck)
{
	Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> (&std::cout);

	for (float i = 0; i < SIM_END_TIME; i+=0.1)
	{
		Simulator::Schedule (Seconds(i), &FindTimeToConvergenceinNode, nodesToCheck, routingStream, Time::S, PRINT_DEBUG);
	}
}

#endif // RIP_SIM_HELPER
