/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2012 Hajime Tazaki, NICT
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hajime Tazaki <tazaki@nict.go.jp>
 */

#include "ns3/network-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/dce-module.h"
#include "ns3/quagga-helper.h"
#include "ns3/point-to-point-helper.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("DceQuaggaOspfd");

// Parameters
uint32_t stopTime = 210;


// Create a ping application that will send traffic to pingDestination from pingSrc. 
static inline void PingSetup(char* pingDestination, Ptr<Node> pingSrc, int packetSize, Time interPacketInterval, bool showPings, Time pingStartTime, Time pingEndTime)
{
	V4PingHelper ping (pingDestination);

	ping.SetAttribute ("Interval", TimeValue (interPacketInterval));
	ping.SetAttribute ("Size", UintegerValue (packetSize));
	if (showPings)
	{
		ping.SetAttribute ("Verbose", BooleanValue (true));
	}
	ApplicationContainer apps = ping.Install (pingSrc);
	apps.Start (pingStartTime);
	apps.Stop (pingEndTime);
}


static inline void OneSideDown(NetDeviceContainer link, int dNum)
{  
  // Get the link to the CSMA channel for the first node
  Ptr<CsmaNetDevice> csma_ptr =  DynamicCast<CsmaNetDevice> (link.Get(dNum));
  if (csma_ptr) {
    //std::cout << "Got csma net device side down\n";
    
    // Disable the CSMA channel for this node
    csma_ptr->SetReceiveEnable(false);
    csma_ptr->SetSendEnable(false);
  }
}

static inline void OneSideUp(NetDeviceContainer link, int dNum)
{
  // Get the link to the CSMA channel for the first node
  Ptr<CsmaNetDevice> csma_ptr =  DynamicCast<CsmaNetDevice> (link.Get(dNum));
  if (csma_ptr) {
    //std::cout << "Got csma net device side up\n";
    
    // Enable the CSMA channel for this node
    csma_ptr->SetReceiveEnable(true);
    csma_ptr->SetSendEnable(true);
  }
}


static inline void LinkDown(NetDeviceContainer link)
{
  OneSideDown(link, 0);
  OneSideDown(link, 1);
}

static inline void LinkUp(NetDeviceContainer link)
{
  OneSideUp(link, 0);
  OneSideUp(link, 1);
}

static inline void NodeDown (int downTime,
				NetDeviceContainer link1, int dNum1,
				NetDeviceContainer link2, int dNum2,
                                NetDeviceContainer link3, int dNum3,
				NetDeviceContainer link4, int dNum4)
{
  Simulator::Schedule (Seconds(downTime), &OneSideDown, link1, dNum1);
  Simulator::Schedule (Seconds(downTime), &OneSideDown, link2, dNum2);
  Simulator::Schedule (Seconds(downTime), &OneSideDown, link3, dNum3);
  Simulator::Schedule (Seconds(downTime), &OneSideDown, link4, dNum4);
 
}

static inline void NodeUp (int upTime,
				NetDeviceContainer link1, int dNum1,
				NetDeviceContainer link2, int dNum2,
                                NetDeviceContainer link3, int dNum3,
				NetDeviceContainer link4, int dNum4)
{
  Simulator::Schedule (Seconds(upTime), &OneSideUp, link1, dNum1);
  Simulator::Schedule (Seconds(upTime), &OneSideUp, link2, dNum2);
  Simulator::Schedule (Seconds(upTime), &OneSideUp, link3, dNum3);
  Simulator::Schedule (Seconds(upTime), &OneSideUp, link4, dNum4);
}

void run_ospf_csma(void)
{
  // Create routers
  Ptr<Node> a = CreateObject<Node> ();
  Names::Add ("RouterA", a);
  Ptr<Node> b = CreateObject<Node> ();
  Names::Add ("RouterB", b);
  Ptr<Node> c = CreateObject<Node> ();
  Names::Add ("RouterC", c);
  Ptr<Node> d = CreateObject<Node> ();
  Names::Add ("RouterD", d);
  Ptr<Node> e = CreateObject<Node> ();
  Names::Add ("RouterE", e);
  Ptr<Node> f = CreateObject<Node> ();
  Names::Add ("RouterF", f);
  Ptr<Node> g = CreateObject<Node> ();
  Names::Add ("RouterG", g);
  Ptr<Node> h = CreateObject<Node> ();
  Names::Add ("RouterH", h);
  
  // Create hosts
  Ptr<Node> host1 = CreateObject<Node> ();
  Names::Add ("HostNode1", host1);
  Ptr<Node> host2 = CreateObject<Node> ();
  Names::Add ("HostNode2", host2);
  Ptr<Node> host3 = CreateObject<Node> ();
  Names::Add ("HostNode3", host3);
  Ptr<Node> host4 = CreateObject<Node> ();
  Names::Add ("HostNode4", host4);
  
  /*
  a - 1	 b - 2		c - 3		d - 4		e - 5
  f - 6	 g - 7        h - 8
  */
  
  // Link hosts to routers
  NodeContainer net11 (host1, a);
  NodeContainer net12 (host2, f);
  NodeContainer net13 (host3, g);
  NodeContainer net14 (host4, e);
  
  // Link routers together
  NodeContainer net0 (a, b);
  NodeContainer net1 (a, c);
  
  NodeContainer net2 (b, c);
  NodeContainer net3 (b, f);
  NodeContainer net5 (b, d);
  
  NodeContainer net4 (f, d);
  
  NodeContainer net6 (c, e);
  
  NodeContainer net7 (d, e);
  NodeContainer net8 (d, g);
  
  NodeContainer net9 (e, h);
  
  NodeContainer net10 (g, h);
  
  // Define routers
  NodeContainer routers1 (a, b, c, d);
  NodeContainer routers2 (e, f, g, h);
  NodeContainer routers (routers1, routers2);  

  // Define Hosts
  NodeContainer people (host1, host2, host3, host4);

  //      NS_LOG_INFO ("Create channels.");
  // Create CSMA channels
  CsmaHelper csma;
  csma.SetChannelAttribute ("DataRate", DataRateValue (5000000));
  csma.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (2)));
  NetDeviceContainer ndc0 = csma.Install (net0);
  NetDeviceContainer ndc1 = csma.Install (net1);
  NetDeviceContainer ndc2 = csma.Install (net2);
  NetDeviceContainer ndc3 = csma.Install (net3);
  NetDeviceContainer ndc4 = csma.Install (net4);
  NetDeviceContainer ndc5 = csma.Install (net5);
  NetDeviceContainer ndc6 = csma.Install (net6);
  NetDeviceContainer ndc7 = csma.Install (net7);
  NetDeviceContainer ndc8 = csma.Install (net8);
  NetDeviceContainer ndc9 = csma.Install (net9);
  NetDeviceContainer ndc10 = csma.Install (net10);
  NetDeviceContainer ndc11 = csma.Install (net11);
  NetDeviceContainer ndc12 = csma.Install (net12);
  NetDeviceContainer ndc13 = csma.Install (net13);
  NetDeviceContainer ndc14 = csma.Install (net14);


  // Schedule a link down
  //Simulator::Schedule (Seconds(70), &LinkDown, ndc8);

  // Schedule a link up
//  Simulator::Schedule (Seconds(75), &LinkUp, ndc8);


  int downTime = 85;
  int upTime = 150;
  NetDeviceContainer link1 = ndc4; // Node d
  NetDeviceContainer link2 = ndc5;
  NetDeviceContainer link3 = ndc7;
  NetDeviceContainer link4 = ndc8;
  int d1 = 1; 
  int d2 = 1;
  int d3 = 0; 
  int d4 = 0;
  NodeDown(downTime, link1, d1, link2, d2, link3, d3, link4, d4);
  NodeUp (upTime, link1, d1, link2, d2, link3, d3, link4, d4);



  // Parts required for use with DCE Quagga OSPF
  // Shouldnt need to change the section below between comments
  //////////////////////////////////////////////////////////////
  DceManagerHelper processManager;
  Ipv4DceRoutingHelper ipv4RoutingHelper;
   
  // Internet stack install
  InternetStackHelper stack;    // IPv4 is required for GlobalRouteMan
  stack.SetRoutingHelper (ipv4RoutingHelper);
  stack.Install (routers);
  
  InternetStackHelper people_stack;
  people_stack.SetIpv6StackInstall (false);
  people_stack.Install (people);

  //Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

  processManager.SetNetworkStack ("ns3::Ns3SocketFdFactory");
  processManager.Install (routers);
  //////////////////////////////////////////////////////////////


  // Make the routers OSPF routers
  QuaggaHelper quagga0;
  quagga0.EnableOspf (net0, "10.0.0.0/24");
  quagga0.EnableOspfDebug (net0);
  quagga0.EnableZebraDebug (net0);
  quagga0.Install (net0);
  
  QuaggaHelper quagga1;
  quagga1.EnableOspf (net1, "10.0.1.0/24");
  quagga1.EnableOspfDebug (net1);
  quagga1.EnableZebraDebug (net1);
  quagga1.Install (net1);

  QuaggaHelper quagga2;
  quagga2.EnableOspf (net2, "10.0.2.0/24");
  quagga2.EnableOspfDebug (net2);
  quagga2.EnableZebraDebug (net2);
  quagga2.Install (net2);
  
  QuaggaHelper quagga3;
  quagga3.EnableOspf (net3, "10.0.3.0/24");
  quagga3.EnableOspfDebug (net3);
  quagga3.EnableZebraDebug (net3);
  quagga3.Install (net3);

  QuaggaHelper quagga4;
  quagga4.EnableOspf (net4, "10.0.4.0/24");
  quagga4.EnableOspfDebug (net4);
  quagga4.EnableZebraDebug (net4);
  quagga4.Install (net4);

  QuaggaHelper quagga5;
  quagga5.EnableOspf (net5, "10.0.5.0/24");
  quagga5.EnableOspfDebug (net5);
  quagga5.EnableZebraDebug (net5);
  quagga5.Install (net5);

  QuaggaHelper quagga6;
  quagga6.EnableOspf (net6, "10.0.6.0/24");
  quagga6.EnableOspfDebug (net6);
  quagga6.EnableZebraDebug (net6);
  quagga6.Install (net6);
  
  QuaggaHelper quagga7;
  quagga7.EnableOspf (net7, "10.0.7.0/24");
  quagga7.EnableOspfDebug (net7);
  quagga7.EnableZebraDebug (net7);
  quagga7.Install (net7);
  
  QuaggaHelper quagga8;
  quagga8.EnableOspf (net8, "10.0.8.0/24");
  quagga8.EnableOspfDebug (net8);
  quagga8.EnableZebraDebug (net8);
  quagga8.Install (net8);
  
  QuaggaHelper quagga9;
  quagga9.EnableOspf (net9, "10.0.9.0/24");
  quagga9.EnableOspfDebug (net9);
  quagga9.EnableZebraDebug (net9);
  quagga9.Install (net9);
  
  QuaggaHelper quagga10;
  quagga10.EnableOspf (net10, "10.0.10.0/24");
  quagga10.EnableOspfDebug (net10);
  quagga10.EnableZebraDebug (net10);
  quagga10.Install (net10);
/*  
  QuaggaHelper quagga11;
  quagga11.EnableOspf (net11, "10.0.11.0/24");
  quagga11.EnableOspfDebug (net11);
  quagga11.EnableZebraDebug (net11);
  quagga11.Install (net11);
  
  QuaggaHelper quagga12;
  quagga12.EnableOspf (net12, "10.0.12.0/24");
  quagga12.EnableOspfDebug (net12);
  quagga12.EnableZebraDebug (net12);
  quagga12.Install (net12);
  
  QuaggaHelper quagga13;
  quagga13.EnableOspf (net13, "10.0.13.0/24");
  quagga13.EnableOspfDebug (net13);
  quagga13.EnableZebraDebug (net13);
  quagga13.Install (net13);
  
  QuaggaHelper quagga14;
  quagga14.EnableOspf (net14, "10.0.14.0/24");
  quagga14.EnableOspfDebug (net14);
  quagga14.EnableZebraDebug (net14);
  quagga14.Install (net14);
  
  QuaggaHelper quagga15;
  quagga15.EnableOspf (net15, "10.0.15.0/24");
  quagga15.EnableOspfDebug (net15);
  quagga15.EnableZebraDebug (net15);
  quagga15.Install (net15);
  
  QuaggaHelper quagga16;
  quagga16.EnableOspf (net16, "10.0.16.0/24");
  quagga16.EnableOspfDebug (net16);
  quagga16.EnableZebraDebug (net16);
  quagga16.Install (net16);
  
  QuaggaHelper quagga17;
  quagga17.EnableOspf (net17, "10.0.17.0/24");
  quagga17.EnableOspfDebug (net17);
  quagga17.EnableZebraDebug (net17);
  quagga17.Install (net17);
  
  QuaggaHelper quagga18;
  quagga18.EnableOspf (net18, "10.0.18.0/24");
  quagga18.EnableOspfDebug (net18);
  quagga18.EnableZebraDebug (net18);
  quagga18.Install (net18);
  
  QuaggaHelper quagga19;
  quagga19.EnableOspf (net19, "10.0.19.0/24");
  quagga19.EnableOspfDebug (net19);
  quagga19.EnableZebraDebug (net19);
  quagga19.Install (net19);
*/



  // Assign addresses.
  // The source and destination networks have global addresses
  // The "core" network just needs link-local addresses for routing.
  // We assign global addresses to the routers as well to receive
  // ICMPv6 errors.
  //NS_LOG_INFO ("Assign IPv4 Addresses.");
  Ipv4AddressHelper ipv4;

  ipv4.SetBase (Ipv4Address ("10.0.0.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic0 = ipv4.Assign (ndc0);

  ipv4.SetBase (Ipv4Address ("10.0.1.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic1 = ipv4.Assign (ndc1);

  ipv4.SetBase (Ipv4Address ("10.0.2.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic2 = ipv4.Assign (ndc2);

  ipv4.SetBase (Ipv4Address ("10.0.3.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic3 = ipv4.Assign (ndc3);

  ipv4.SetBase (Ipv4Address ("10.0.4.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic4 = ipv4.Assign (ndc4);

  ipv4.SetBase (Ipv4Address ("10.0.5.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic5 = ipv4.Assign (ndc5);

  ipv4.SetBase (Ipv4Address ("10.0.6.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic6 = ipv4.Assign (ndc6);
  
  ipv4.SetBase (Ipv4Address ("10.0.7.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic7 = ipv4.Assign (ndc7);

  ipv4.SetBase (Ipv4Address ("10.0.8.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic8 = ipv4.Assign (ndc8);

  ipv4.SetBase (Ipv4Address ("10.0.9.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic9 = ipv4.Assign (ndc9);

  ipv4.SetBase (Ipv4Address ("10.0.10.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic10 = ipv4.Assign (ndc10);

  ipv4.SetBase (Ipv4Address ("10.0.11.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic11 = ipv4.Assign (ndc11);

  ipv4.SetBase (Ipv4Address ("10.0.12.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic12 = ipv4.Assign (ndc12);

  ipv4.SetBase (Ipv4Address ("10.0.13.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic13 = ipv4.Assign (ndc13);
  
  ipv4.SetBase (Ipv4Address ("10.0.14.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic14 = ipv4.Assign (ndc14);


  // Assign static addresses for source and destination
  Ptr<Ipv4StaticRouting> staticRouting;
  staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> (host1->GetObject<Ipv4> ()->GetRoutingProtocol ());
  staticRouting->SetDefaultRoute ("10.0.11.2", 1 );
  staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> (host2->GetObject<Ipv4> ()->GetRoutingProtocol ());
  staticRouting->SetDefaultRoute ("10.0.12.2", 1 );
  staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> (host3->GetObject<Ipv4> ()->GetRoutingProtocol ());
  staticRouting->SetDefaultRoute ("10.0.13.2", 1 );
  staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> (host4->GetObject<Ipv4> ()->GetRoutingProtocol ());
  staticRouting->SetDefaultRoute ("10.0.14.2", 1 );


  // Enable tracing
  AsciiTraceHelper ascii;
  csma.EnableAsciiAll (ascii.CreateFileStream ("output_uk/uk-ospf.tr"));
  csma.EnablePcapAll ("output_uk/uk-ospf", true);

  // Trace routing tables
  Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> ("output_uk/uk-routing-tables.log", std::ios::out);
  ipv4RoutingHelper.PrintRoutingTableAllEvery (Seconds (10), routingStream);


  // Create ping application
/*  bool showPings = true;
  uint32_t packetSize = 1024;
  Time interPacketInterval = Seconds (1.0);
  Time endPing = Seconds (200.0);

  char pingDest[] = "10.0.9.2";
  PingSetup(pingDest, host1, packetSize, interPacketInterval, showPings, Seconds (1.0), endPing);
  
  char pingDest2[] = "10.0.13.1";
  PingSetup(pingDest2, host1, packetSize, interPacketInterval, showPings, Seconds (1.0), endPing);

  char pingDest3[] = "10.0.10.2";
  PingSetup(pingDest3, host2, packetSize, interPacketInterval, showPings, Seconds (1.0), endPing);

  char pingDest5[] = "10.0.3.2";
  PingSetup(pingDest5, host3, packetSize, interPacketInterval, showPings, Seconds (1.0), endPing);

  char pingDest6[] = "10.0.11.1";
  PingSetup(pingDest6, host4, packetSize, interPacketInterval, showPings, Seconds (1.0), endPing);
*/

}

static inline void PrintSimulationTime(ns3::Time printTime)
{
  std::cout << "It is now at time " << Now ().As(Time::S) << " in the simulation\n";
} 

int main (int argc, char *argv[])
{
  RngSeedManager::SetSeed(11); // Used default, 123, 539, 1001


  // Run the simulation
  run_ospf_csma();

  // Print out the time in the simulation so that I can track the progress
  for (uint32_t currentRunTime = 0; currentRunTime <= stopTime; currentRunTime += 25)
  {
    Simulator::Schedule (Seconds((float) currentRunTime), &PrintSimulationTime, Seconds ((float) currentRunTime));
  }

  if (stopTime != 0)
    {
      Simulator::Stop (Seconds (stopTime));
    }
  Simulator::Run ();
  Simulator::Destroy ();

  return 0;
}
