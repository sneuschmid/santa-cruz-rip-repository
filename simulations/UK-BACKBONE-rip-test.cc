/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 Universita' di Firenze, Italy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Spencer Neuschmid
 */

// Network topology
//
// The network topology is a medium sized network with 10 nodes each having a 
// distance metric of one to its neighboring nodes. 
//    SRC
//     |<=== source network
//
//
// All nodes are RIPv2 routers.
//
// Test setup:
//
// The test will set up the nodes and wait for the network to come to convergence. Once
// this occurs, pings will be sent out through the network. At 100 seconds into the simulation,
// multiple links will be cut and the network will be monitored to see how it recovered. 
// 
//
// If "showPings" is enabled, the user will see:
// 1) if the ping has been acknowledged
// 2) if a Destination Unreachable has been received by the sender
// 3) nothing, when the Echo Request has been received by the destination but
//    the Echo Reply is unable to reach the sender.
// Examining the .pcap files with Wireshark can confirm this effect.


#include <fstream>
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-routing-table-entry.h"

// Include helpful simulation functions
#define SIM_END_TIME (500)

#include "../scratch/rip_sim_helpers.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("RipSimpleRouting");


int main (int argc, char **argv)
{
  // Set a random seed for the simulation.
  RngSeedManager::SetSeed(123);
#if 1	
  bool verbose = false;
  bool printRoutingTables = false;
  bool showPings = false;
  bool showConvergence = false;
  bool rip = false;
  bool dt = false;
  std::string SplitHorizon ("PoisonReverse");

  // Specify command line inputs into the simulation. 
  CommandLine cmd;
  cmd.AddValue ("verbose", "turn on log components", verbose);
  cmd.AddValue ("printRoutingTables", "Print routing tables at 30, 60 and 90 seconds", printRoutingTables);
  cmd.AddValue ("showPings", "Show Ping6 reception", showPings);
  cmd.AddValue ("showConvergence", "Show when nodes reach convergence", showConvergence);
  cmd.AddValue ("rip", "Show RIP control messages", rip);
  cmd.AddValue ("splitHorizonStrategy", "Split Horizon strategy to use (NoSplitHorizon, SplitHorizon, PoisonReverse)", SplitHorizon);
  cmd.AddValue ("useDT", "Use Distance tables to route packets", dt);
  cmd.Parse (argc, argv);

  if (rip)
  {
	LogComponentEnable ("RipSimpleRouting", LOG_LEVEL_INFO);
    LogComponentEnable ("Rip", LOG_LEVEL_ALL);
  }

  if (verbose)
    {
      LogComponentEnableAll (LogLevel (LOG_PREFIX_TIME | LOG_PREFIX_NODE));
      LogComponentEnable ("RipSimpleRouting", LOG_LEVEL_INFO);
      LogComponentEnable ("Rip", LOG_LEVEL_ALL);
      LogComponentEnable ("Ipv4Interface", LOG_LEVEL_ALL);
      LogComponentEnable ("Icmpv4L4Protocol", LOG_LEVEL_ALL);
      LogComponentEnable ("Ipv4L3Protocol", LOG_LEVEL_ALL);
      LogComponentEnable ("ArpCache", LOG_LEVEL_ALL);
      LogComponentEnable ("V4Ping", LOG_LEVEL_ALL);
    }

  if (SplitHorizon == "NoSplitHorizon")
    {
      Config::SetDefault ("ns3::Rip::SplitHorizon", EnumValue (RipNg::NO_SPLIT_HORIZON));
    }
  else if (SplitHorizon == "SplitHorizon")
    {
      Config::SetDefault ("ns3::Rip::SplitHorizon", EnumValue (RipNg::SPLIT_HORIZON));
    }
  else
    {
      Config::SetDefault ("ns3::Rip::SplitHorizon", EnumValue (RipNg::POISON_REVERSE));
    }
	
  NS_LOG_INFO ("Create routers.");
  Ptr<Node> a = CreateObject<Node> ();
  Names::Add ("RouterA", a);
  Ptr<Node> b = CreateObject<Node> ();
  Names::Add ("RouterB", b);
  Ptr<Node> c = CreateObject<Node> ();
  Names::Add ("RouterC", c);
  Ptr<Node> d = CreateObject<Node> ();
  Names::Add ("RouterD", d);
  Ptr<Node> e = CreateObject<Node> ();
  Names::Add ("RouterE", e);
  Ptr<Node> f = CreateObject<Node> ();
  Names::Add ("RouterF", f);
  Ptr<Node> g = CreateObject<Node> ();
  Names::Add ("RouterG", g);
  Ptr<Node> h = CreateObject<Node> ();
  Names::Add ("RouterH", h);
  
  NS_LOG_INFO ("Create hosts.");
  Ptr<Node> host1 = CreateObject<Node> ();
  Names::Add ("HostNode1", host1);
  Ptr<Node> host2 = CreateObject<Node> ();
  Names::Add ("HostNode2", host2);
  Ptr<Node> host3 = CreateObject<Node> ();
  Names::Add ("HostNode3", host3);
  Ptr<Node> host4 = CreateObject<Node> ();
  Names::Add ("HostNode4", host4);
  
  /*
  a - 1	 b - 2		c - 3		d - 4		e - 5
  f - 6	 g - 7        h - 8
  */
  
  // Link hosts to routers
  NodeContainer net11 (host1, a);
  NodeContainer net12 (host2, f);
  NodeContainer net13 (host3, g);
  NodeContainer net14 (host4, e);
  
  // Link routers together
  NodeContainer net0 (a, b);
  NodeContainer net1 (a, c);
  
  NodeContainer net2 (b, c);
  NodeContainer net3 (b, f);
  NodeContainer net5 (b, d);
  
  NodeContainer net4 (f, d);
  
  NodeContainer net6 (c, e);
  
  NodeContainer net7 (d, e);
  NodeContainer net8 (d, g);
  
  NodeContainer net9 (e, h);
  
  NodeContainer net10 (g, h);
  
  // Define routers
  NodeContainer routers1 (a, b, c, d);
  NodeContainer routers2 (e, f, g, h);
  
  // Define Hosts
  NodeContainer nodes (host1, host2, host3, host4);

  // Create link layer channels.
  NS_LOG_INFO ("Create channels.");
  CsmaHelper csma;
  csma.SetChannelAttribute ("DataRate", DataRateValue (5000000));
  csma.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (2)));
  NetDeviceContainer ndc0   = csma.Install (net0);
  NetDeviceContainer ndc1   = csma.Install (net1);
  NetDeviceContainer ndc2   = csma.Install (net2);
  NetDeviceContainer ndc3   = csma.Install (net3);
  NetDeviceContainer ndc4   = csma.Install (net4);
  NetDeviceContainer ndc5   = csma.Install (net5);
  NetDeviceContainer ndc6   = csma.Install (net6);
  NetDeviceContainer ndc7   = csma.Install (net7);
  NetDeviceContainer ndc8   = csma.Install (net8);
  NetDeviceContainer ndc9   = csma.Install (net9);
  NetDeviceContainer ndc10 = csma.Install (net10);
  NetDeviceContainer ndc11 = csma.Install (net11);
  NetDeviceContainer ndc12 = csma.Install (net12);
  NetDeviceContainer ndc13 = csma.Install (net13);
  NetDeviceContainer ndc14 = csma.Install (net14);
  
  NS_LOG_INFO ("Create IPv4 and routing");
  RipHelper ripRouting;

  // Rule of thumb:
  // Interfaces are added sequentially, starting from 0
  // However, interface 0 is always the loopback...
  // Exclude interfaces connected to hosts that will be using static routing.
  /*ripRouting.ExcludeInterface (a, 1);
  ripRouting.ExcludeInterface (d, 1);
  ripRouting.ExcludeInterface (h, 1);
  ripRouting.ExcludeInterface (k, 1);
  ripRouting.ExcludeInterface (j, 1);*/

  Ipv4ListRoutingHelper listRH;
  listRH.Add (ripRouting, 0);

  InternetStackHelper internet;
  internet.SetIpv6StackInstall (false);
  internet.SetRoutingHelper (listRH);
  internet.Install (routers1);
  internet.Install (routers2);

  InternetStackHelper internetNodes;
  internetNodes.SetIpv6StackInstall (false);
  internetNodes.Install (nodes);

  // Assign addresses.
  // The source and destination networks have global addresses
  // The "core" network just needs link-local addresses for routing.
  // We assign global addresses to the routers as well to receive
  // ICMPv6 errors.
  NS_LOG_INFO ("Assign IPv4 Addresses.");
  Ipv4AddressHelper ipv4;

  ipv4.SetBase (Ipv4Address ("10.0.0.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic0 = ipv4.Assign (ndc0);

  ipv4.SetBase (Ipv4Address ("10.0.1.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic1 = ipv4.Assign (ndc1);

  ipv4.SetBase (Ipv4Address ("10.0.2.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic2 = ipv4.Assign (ndc2);

  ipv4.SetBase (Ipv4Address ("10.0.3.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic3 = ipv4.Assign (ndc3);

  ipv4.SetBase (Ipv4Address ("10.0.4.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic4 = ipv4.Assign (ndc4);

  ipv4.SetBase (Ipv4Address ("10.0.5.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic5 = ipv4.Assign (ndc5);

  ipv4.SetBase (Ipv4Address ("10.0.6.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic6 = ipv4.Assign (ndc6);
  
  ipv4.SetBase (Ipv4Address ("10.0.7.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic7 = ipv4.Assign (ndc7);

  ipv4.SetBase (Ipv4Address ("10.0.8.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic8 = ipv4.Assign (ndc8);

  ipv4.SetBase (Ipv4Address ("10.0.9.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic9 = ipv4.Assign (ndc9);

  ipv4.SetBase (Ipv4Address ("10.0.10.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic10 = ipv4.Assign (ndc10);

  ipv4.SetBase (Ipv4Address ("10.0.11.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic11 = ipv4.Assign (ndc11);

  ipv4.SetBase (Ipv4Address ("10.0.12.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic12 = ipv4.Assign (ndc12);

  ipv4.SetBase (Ipv4Address ("10.0.13.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic13 = ipv4.Assign (ndc13);
  
  ipv4.SetBase (Ipv4Address ("10.0.14.0"), Ipv4Mask ("255.255.255.0"));
  Ipv4InterfaceContainer iic14 = ipv4.Assign (ndc14);

  Ptr<Ipv4StaticRouting> staticRouting;
  staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> (host1->GetObject<Ipv4> ()->GetRoutingProtocol ());
  staticRouting->SetDefaultRoute ("10.0.11.2", 1 );
  staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> (host2->GetObject<Ipv4> ()->GetRoutingProtocol ());
  staticRouting->SetDefaultRoute ("10.0.12.2", 1 );
  staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> (host3->GetObject<Ipv4> ()->GetRoutingProtocol ());
  staticRouting->SetDefaultRoute ("10.0.13.2", 1 );
  staticRouting = Ipv4RoutingHelper::GetRouting <Ipv4StaticRouting> (host4->GetObject<Ipv4> ()->GetRoutingProtocol ());
  staticRouting->SetDefaultRoute ("10.0.14.2", 1 );
#endif
  // Optionally, print out the routing tables at specified times to help debugging.
  if (printRoutingTables)
    {
      RipHelper routingHelper;

      Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> (&std::cout);

	  // double firstTime = 79;
	  // double secondTime = 50;
	  // double thirdTime = 90;
	  // double fourthTime = 100;
	  double finalTime = SIM_END_TIME;

      uint8_t numRouters = 8;
	  Ptr<Node> routersToPrint[numRouters] = {a, b, c, d, e, f, g, h};
	
	  // PrintRouteTables(firstTime, routersToPrint, numRouters, routingStream);
	  // PrintRouteTables(secondTime, routersToPrint, numRouters, routingStream);
	  // PrintRouteTables(thirdTime, routersToPrint, numRouters, routingStream);
	  // PrintRouteTables(fourthTime, routersToPrint, numRouters, routingStream);
	  PrintRouteTables(finalTime, routersToPrint, numRouters, routingStream);

   }
#if 1
  // Create the ping applications that will simulate application traffic.
  NS_LOG_INFO ("Create Applications.");
  uint32_t packetSize = 1024;
  Time interPacketInterval = Seconds (1.0);

  Time endPing = Seconds(200.0);

  char pingDest[] = "10.0.9.2";
  PingSetup(pingDest, host1, packetSize, interPacketInterval, showPings, Seconds (1.0), endPing);
  
  char pingDest2[] = "10.0.13.1";
  PingSetup(pingDest2, host1, packetSize, interPacketInterval, showPings, Seconds (1.0), endPing);

  char pingDest3[] = "10.0.10.2";
  PingSetup(pingDest3, host2, packetSize, interPacketInterval, showPings, Seconds (1.0), endPing);

  char pingDest5[] = "10.0.3.2";
  PingSetup(pingDest5, host3, packetSize, interPacketInterval, showPings, Seconds (1.0), endPing);

  char pingDest6[] = "10.0.11.1";
  PingSetup(pingDest6, host4, packetSize, interPacketInterval, showPings, Seconds (1.0), endPing);

  AsciiTraceHelper ascii;
  csma.EnableAsciiAll (ascii.CreateFileStream ("test_output/UK-BACKBONE-routing.tr"));
  csma.EnablePcapAll ("test_output/UK-BACKBONE-routing", true);
#endif
  // Tear down and bring up links at regular intervals.
  // Simulator::Schedule (Seconds (40), &TearDownLink, d, g, 4, 1);
  // Simulator::Schedule (Seconds (80), &BringUpLink, d, g, 4, 1);
  
  // uint32_t numIfaces = 4;
  // uint32_t ifaces[] = {1, 2, 3, 4};
  // Simulator::Schedule (Seconds (40), &NodeDown, d, ifaces, numIfaces);
  
  // Simulator::Schedule (Seconds (80), &NodeUp, d, ifaces, numIfaces);
  
  // Print out the overhead statistics at the end of the simulation.
  NodeContainer routers(routers1, routers2);
  Simulator::Schedule (Seconds(SIM_END_TIME), &PrintRipStats, routers);
  
  // If the user asks for convergence, then print out each time a router reaches convergence.
  if (showConvergence)
  {
	CheckConvergence(routers);
  }
  
  UseDistanceTables(0.0, routers, dt);
  //PrintDistanceTables((double) 41, routers, Create<OutputStreamWrapper> (&std::cout));
  
  // Now, do the actual simulation.
  NS_LOG_INFO ("Run Simulation.");
  Simulator::Stop (Seconds (SIM_END_TIME));
  Simulator::Run ();
  Simulator::Destroy ();
  NS_LOG_INFO ("Done.");
}

