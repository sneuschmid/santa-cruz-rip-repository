/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
* Copyright (c) 2016 Universita' di Firenze, Italy
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation;
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Author: Tommaso Pecorella <tommaso.pecorella@unifi.it>
*/

#include <iomanip>
#include "scrip.h"
#include "ns3/log.h"
#include "ns3/abort.h"
#include "ns3/assert.h"
#include "ns3/unused.h"
#include "ns3/random-variable-stream.h"
#include "ns3/ipv4-route.h"
#include "ns3/node.h"
#include "ns3/names.h"
#include "ns3/rip-header.h"
#include "ns3/udp-header.h"
#include "ns3/enum.h"
#include "ns3/uinteger.h"
#include "ns3/ipv4-packet-info-tag.h"
#include "ns3/loopback-net-device.h"

#define RIP_ALL_NODE "224.0.0.9"
#define RIP_PORT 520

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("Rip");

NS_OBJECT_ENSURE_REGISTERED (Rip);

Rip::Rip ()
: m_ipv4 (0), m_splitHorizonStrategy (Rip::POISON_REVERSE), m_initialized (false)
{
    m_rng = CreateObject<UniformRandomVariable> ();
}

Rip::~Rip ()
{
}

TypeId
Rip::GetTypeId (void)
{
    static TypeId tid = TypeId ("ns3::Rip")
    .SetParent<Ipv4RoutingProtocol> ()
    .SetGroupName ("Internet")
    .AddConstructor<Rip> ()
    .AddAttribute ("UnsolicitedRoutingUpdate", "The time between two Unsolicited Routing Updates.",
        TimeValue (Seconds(10)),
        MakeTimeAccessor (&Rip::m_unsolicitedUpdate),
        MakeTimeChecker ())
    .AddAttribute ("StartupDelay", "Maximum random delay for protocol startup (send route requests).",
        TimeValue (Seconds(1)),
        MakeTimeAccessor (&Rip::m_startupDelay),
        MakeTimeChecker ())
    .AddAttribute ("TimeoutDelay", "The delay to invalidate a route.",
        TimeValue (Seconds(40)),
        MakeTimeAccessor (&Rip::m_timeoutDelay),
        MakeTimeChecker ())
    .AddAttribute ("GarbageCollectionDelay", "The delay to delete an expired route.",
        TimeValue (Seconds(120)),
        MakeTimeAccessor (&Rip::m_garbageCollectionDelay),
        MakeTimeChecker ())
    .AddAttribute ("MinTriggeredCooldown", "Min cooldown delay after a Triggered Update.",
        TimeValue (Seconds(0.00)),
        MakeTimeAccessor (&Rip::m_minTriggeredUpdateDelay),
        MakeTimeChecker ())
    .AddAttribute ("MaxTriggeredCooldown", "Max cooldown delay after a Triggered Update.",
        TimeValue (Seconds(0.10)),
        MakeTimeAccessor (&Rip::m_maxTriggeredUpdateDelay),
        MakeTimeChecker ())
    .AddAttribute ("SplitHorizon", "Split Horizon strategy.",
        EnumValue (Rip::POISON_REVERSE),
        MakeEnumAccessor (&Rip::m_splitHorizonStrategy),
        MakeEnumChecker (Rip::NO_SPLIT_HORIZON, "NoSplitHorizon",
        Rip::SPLIT_HORIZON, "SplitHorizon",
        Rip::POISON_REVERSE, "PoisonReverse"))
    .AddAttribute ("LinkDownValue", "Value for link down in count to infinity.",
        UintegerValue (100), // essentially infinite
        MakeUintegerAccessor (&Rip::m_linkDown),
        MakeUintegerChecker<uint32_t> ())
    // This is required since pending request tables have not been added yet to ensure that
    // request messages are not endlessly forwarded and that once one request is received, 
    // others will not be fordwarded witht hat same destination. 
    .AddAttribute ("ForwardingDelay", "Delay after a request is forwarded.",
        TimeValue (Seconds(1)),
        MakeTimeAccessor (&Rip::m_forwardingDelay),
        MakeTimeChecker ())
    ;
    return tid;
}

int64_t Rip::AssignStreams (int64_t stream)
{
    NS_LOG_FUNCTION (this << stream);

    m_rng->SetStream (stream);
    return 1;
}

void Rip::DoInitialize ()
{
    NS_LOG_FUNCTION (this);

    bool addedGlobal = false;

    m_initialized = true;

    Time delay = m_unsolicitedUpdate + Seconds (m_rng->GetValue (0, 0.5*m_unsolicitedUpdate.GetSeconds ()) );
    m_nextUnsolicitedUpdate = Simulator::Schedule (delay, &Rip::SendUnsolicitedRouteUpdate, this);


    for (uint32_t i = 0 ; i < m_ipv4->GetNInterfaces (); i++)
    {
        Ptr<LoopbackNetDevice> check = DynamicCast<LoopbackNetDevice> (m_ipv4->GetNetDevice (i));
        if (check)
        {
            continue;
        }

        bool activeInterface = false;
        if (m_interfaceExclusions.find (i) == m_interfaceExclusions.end ())
        {
            activeInterface = true;
            m_ipv4->SetForwarding (i, true);
        }

        for (uint32_t j = 0; j < m_ipv4->GetNAddresses (i); j++)
        {
            Ipv4InterfaceAddress address = m_ipv4->GetAddress (i, j);
            if (address.GetScope() != Ipv4InterfaceAddress::HOST && activeInterface == true)
            {
                NS_LOG_LOGIC ("RIP: adding socket to " << address.GetLocal ());
                TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
                Ptr<Node> theNode = GetObject<Node> ();
                Ptr<Socket> socket = Socket::CreateSocket (theNode, tid);
                InetSocketAddress local = InetSocketAddress (address.GetLocal (), RIP_PORT);
                socket->BindToNetDevice (m_ipv4->GetNetDevice (i));
                int ret = socket->Bind (local);
                NS_ASSERT_MSG (ret == 0, "Bind unsuccessful");
                socket->SetIpRecvTtl (true);
                m_sendSocketList[socket] = i;
            }
            else if (m_ipv4->GetAddress (i, j).GetScope() == Ipv4InterfaceAddress::GLOBAL)
            {
                addedGlobal = true;
            }
        }
    }

    if (!m_recvSocket)
    {
        NS_LOG_LOGIC ("RIP: adding receiving socket");
        TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
        Ptr<Node> theNode = GetObject<Node> ();
        m_recvSocket = Socket::CreateSocket (theNode, tid);
        InetSocketAddress local = InetSocketAddress (RIP_ALL_NODE, RIP_PORT);
        m_recvSocket->Bind (local);
        m_recvSocket->SetRecvCallback (MakeCallback (&Rip::Receive, this));
        m_recvSocket->SetIpRecvTtl (true);
        m_recvSocket->SetRecvPktInfo (true);
    }


    if (addedGlobal)
    {
        Time delay = Seconds (m_rng->GetValue (m_minTriggeredUpdateDelay.GetSeconds (), m_maxTriggeredUpdateDelay.GetSeconds ()));
        m_nextTriggeredUpdate = Simulator::Schedule (delay, &Rip::DoSendRouteUpdate, this, false);
    }

    delay = Seconds (m_rng->GetValue (0.01, m_startupDelay.GetSeconds ()));
    m_nextTriggeredUpdate = Simulator::Schedule (delay, &Rip::SendFullTableRequest, this);

    SetForwardRequestTracker(false);	
    //EnableUseDistanceTables(false);

    Ipv4RoutingProtocol::DoInitialize ();
}

Ptr<Ipv4Route> Rip::RouteOutput (Ptr<Packet> p, const Ipv4Header &header, Ptr<NetDevice> oif, Socket::SocketErrno &sockerr)
{
    NS_LOG_FUNCTION (this << header << oif);

    Ipv4Address destination = header.GetDestination ();
    Ptr<Ipv4Route> rtentry = 0;

    if (destination.IsMulticast ())
    {
        // Note:  Multicast routes for outbound packets are stored in the
        // normal unicast table.  An implication of this is that it is not
        // possible to source multicast datagrams on multiple interfaces.
        // This is a well-known property of sockets implementation on
        // many Unix variants.
        // So, we just log it and fall through to LookupStatic ()
        NS_LOG_LOGIC ("RouteOutput (): Multicast destination");
    }

    rtentry = Lookup (destination, oif);

    // Optionally use distance tables to route output
    if (false)
    {
        Ptr<Ipv4Route> rte = DistanceTableLookup(header.GetDestination ());
        if (rte)
        {
            rtentry = rte;
        }
    }
    if (rtentry)
    {
        sockerr = Socket::ERROR_NOTERROR;
    }
    else
    {
        sockerr = Socket::ERROR_NOROUTETOHOST;
    }
    return rtentry;
}

bool Rip::RouteInput (Ptr<const Packet> p, const Ipv4Header &header, Ptr<const NetDevice> idev,
UnicastForwardCallback ucb, MulticastForwardCallback mcb,
LocalDeliverCallback lcb, ErrorCallback ecb)
{
    NS_LOG_FUNCTION (this << p << header << header.GetSource () << header.GetDestination () << idev);

    NS_ASSERT (m_ipv4 != 0);
    // Check if input device supports IP
    NS_ASSERT (m_ipv4->GetInterfaceForDevice (idev) >= 0);
    uint32_t iif = m_ipv4->GetInterfaceForDevice (idev);
    Ipv4Address dst = header.GetDestination ();

    if (m_ipv4->IsDestinationAddress (header.GetDestination (), iif))
    {
        if (!lcb.IsNull ())
        {
            NS_LOG_LOGIC ("Local delivery to " << header.GetDestination ());
            lcb (p, header, iif);
            return true;
        }
        else
        {
            // The local delivery callback is null.  This may be a multicast
            // or broadcast packet, so return false so that another
            // multicast routing protocol can handle it.  It should be possible
            // to extend this to explicitly check whether it is a unicast
            // packet, and invoke the error callback if so
            return false;
        }
    }

    if (dst.IsMulticast ())
    {
        NS_LOG_LOGIC ("Multicast route not supported by RIP");
        return false; // Let other routing protocols try to handle this
    }

    if (header.GetDestination ().IsBroadcast ())
    {
        NS_LOG_LOGIC ("Dropping packet not for me and with dst Broadcast");
        if (!ecb.IsNull ())
        {
            std::cout << "Error occurred trying to route packet 1\n";
            ecb (p, header, Socket::ERROR_NOROUTETOHOST);
        }
        return false;
    }

    // Check if input device supports IP forwarding
    if (m_ipv4->IsForwarding (iif) == false)
    {
        NS_LOG_LOGIC ("Forwarding disabled for this interface");
        if (!ecb.IsNull ())
        {
            std::cout << "Error occurred trying to route packet 1\n";
            ecb (p, header, Socket::ERROR_NOROUTETOHOST);
        }
        return true;
    }
    // Next, try to find a route
    NS_LOG_LOGIC ("Unicast destination");

    Ptr<Ipv4Route> rtentry = Lookup (header.GetDestination ());
    // Optionally use distance tables to route input
    if (m_useDT && (m_distanceTable.GetNumValidDTEntries(header.GetDestination ()) > 1))
    {
        Ptr<Ipv4Route> rte = DistanceTableLookup(header.GetDestination ());
        if (rte)
        {
            rtentry = rte;
        }
    }

    if (rtentry != 0)
    {
        NS_LOG_LOGIC ("Found unicast destination - calling unicast callback");
        ucb (rtentry, p, header);  // unicast forwarding callback
        return true;
    }
    else
    {
        NS_LOG_LOGIC ("Did not find unicast destination - returning false");

        // Did not find destination, return false but also send out a request for the destination
        RipRte reqRTE;
        reqRTE.SetPrefix (header.GetDestination ());
        // This needs to be done because ns3 does not support retrieving masks from ipv4 addresses.
        // This should not be done in practice, but all of my simulations use this network mask, so this is 
        // an easy hacky solution to make the simulations run.
        reqRTE.SetSubnetMask ((Ipv4Mask) "255.255.255.0");
        reqRTE.SetReferenceDistance (m_linkDown); // By default set the reference distance to max
        // Check if the route previously existed, if so then request a new reference distance based on the old one.
        for (RoutesI rtIter = m_routes.begin (); rtIter != m_routes.end (); rtIter++)
        {
            Ipv4Address requestedAddress = reqRTE.GetPrefix ();
            Ipv4Address combinedRTEAddr = requestedAddress.CombineMask (reqRTE.GetSubnetMask ());
            if (combinedRTEAddr == rtIter->first->GetDestNetwork ())
            {
                reqRTE.SetReferenceDistance (rtIter->first->GetReferenceDistance ());
            }
        }
        // Send out arequest for this missing route
        SendIndividualRouteRequest(reqRTE, 0);
        return false; // Let other routing protocols try to handle this
    }
}

void Rip::NotifyInterfaceUp (uint32_t i)
{
    NS_LOG_FUNCTION (this << i);

    Ptr<LoopbackNetDevice> check = DynamicCast<LoopbackNetDevice> (m_ipv4->GetNetDevice (i));
    if (check)
    {
        return;
    }

    for (uint32_t j = 0; j < m_ipv4->GetNAddresses (i); j++)
    {
        Ipv4InterfaceAddress address = m_ipv4->GetAddress (i, j);
        Ipv4Mask networkMask = address.GetMask ();
        Ipv4Address networkAddress = address.GetLocal ().CombineMask (networkMask);

        if (address.GetScope () == Ipv4InterfaceAddress::GLOBAL)
        {
            // Remove previous routes to this destination and add the new one.
            NotifyRemoveAddress(i, address);
            AddNetworkRouteTo (networkAddress, networkMask, i);
        }
    }

    if (!m_initialized)
    {
        return;
    }


    bool sendSocketFound = false;
    for (SocketListI iter = m_sendSocketList.begin (); iter != m_sendSocketList.end (); iter++ )
    {
        if (iter->second == i)
        {
            sendSocketFound = true;
            break;
        }
    }

    bool activeInterface = false;
    if (m_interfaceExclusions.find (i) == m_interfaceExclusions.end ())
    {
        activeInterface = true;
        m_ipv4->SetForwarding (i, true);
    }

    for (uint32_t j = 0; j < m_ipv4->GetNAddresses (i); j++)
    {
        Ipv4InterfaceAddress address = m_ipv4->GetAddress (i, j);

        if (address.GetScope() != Ipv4InterfaceAddress::HOST && sendSocketFound == false && activeInterface == true)
        {
            NS_LOG_LOGIC ("RIP: adding sending socket to " << address.GetLocal ());
            TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
            Ptr<Node> theNode = GetObject<Node> ();
            Ptr<Socket> socket = Socket::CreateSocket (theNode, tid);
            InetSocketAddress local = InetSocketAddress (address.GetLocal (), RIP_PORT);
            socket->BindToNetDevice (m_ipv4->GetNetDevice (i));
            socket->Bind (local);
            socket->SetIpRecvTtl (true);
            m_sendSocketList[socket] = i;
        }
        if (address.GetScope () == Ipv4InterfaceAddress::GLOBAL)
        {
            SendTriggeredRouteUpdate ();
        }
    }

    if (!m_recvSocket)
    {
        NS_LOG_LOGIC ("RIP: adding receiving socket");
        TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
        Ptr<Node> theNode = GetObject<Node> ();
        m_recvSocket = Socket::CreateSocket (theNode, tid);
        InetSocketAddress local = InetSocketAddress (RIP_ALL_NODE, RIP_PORT);
        m_recvSocket->Bind (local);
        m_recvSocket->SetRecvCallback (MakeCallback (&Rip::Receive, this));
        m_recvSocket->SetIpRecvTtl (true);
        m_recvSocket->SetRecvPktInfo (true);
    }

    for (RoutesI it = m_routes.begin (); it != m_routes.end (); it++)
    {
        if (it->first->GetInterface () == i  && it->first->GetRouteMetric () == m_linkDown)
        {
            RipRte reqRTE;
            reqRTE.SetPrefix (it->first->GetDestNetwork ());
            reqRTE.SetSubnetMask ((it->first->GetDestNetworkMask ()));
            reqRTE.SetReferenceDistance (0);//it->first->GetReferenceDistance ());

            // Send out a request for this missing route
            SendIndividualRouteRequest(reqRTE, 0);
        }
       /*  else if ((it->first->GetInterface () != i)  && (it->first->GetRouteMetric () != m_linkDown) && (it->first->GetRouteMetric () - 1 != it->first->GetReferenceDistance ()))
        {
            //std::cout << "Sending out request when interface up for address" << it->first->GetDestNetwork () << "\n";
            RipRte reqRTE;
            reqRTE.SetPrefix (it->first->GetDestNetwork ());
            reqRTE.SetSubnetMask ((it->first->GetDestNetworkMask ()));
            reqRTE.SetReferenceDistance (0);//it->first->GetReferenceDistance ());

            //Send out a request for this missing route
            SendIndividualRouteRequest(reqRTE, 0);
        } */
    }
}

void Rip::NotifyInterfaceDown (uint32_t interface)
{
    NS_LOG_FUNCTION (this << interface);

    /* remove all routes that are going through this interface */
    for (RoutesI it = m_routes.begin (); it != m_routes.end (); it++)
    {
       // Sometimes it gets messed up, we can start over in this case
       if (it->first->GetDestNetworkMask () != "255.255.255.0")
       {
           it = m_routes.begin ();
           continue;
       }
       
        if (it->first->GetInterface () == interface)
        {
            RipDistanceTableEntry dte;
            dte.SetNetwork(it->first->GetDestNetwork ());
            dte.SetNetworkMask(it->first->GetDestNetworkMask ());
            dte.SetRouteInterface(interface);
            
            if (it->first->GetRouteStatus () == RipRoutingTableEntry::RIP_INVALID)
            {
                std::cout << "Interface went down in node " <<  m_ipv4->GetObject<Node> ()->GetId ()  << " for address " << it->first->GetDestNetwork () << "\n";
                InvalidateRoute (it->first);
            }
            
            // Delete the entry from the distance table
            m_distanceTable.DeleteDistanceTableEntry(&dte);
        }
    }

    for (SocketListI iter = m_sendSocketList.begin (); iter != m_sendSocketList.end (); iter++ )
    {
        NS_LOG_INFO ("Checking socket for interface " << interface);
        if (iter->second == interface)
        {
            NS_LOG_INFO ("Removed socket for interface " << interface);
            iter->first->Close ();
            m_sendSocketList.erase (iter);
            break;
        }
    }

    if (m_interfaceExclusions.find (interface) == m_interfaceExclusions.end ())
    {
        SendTriggeredRouteUpdate ();
    }
}

void Rip::NotifyAddAddress (uint32_t interface, Ipv4InterfaceAddress address)
{
    NS_LOG_FUNCTION (this << interface << address);

    if (!m_ipv4->IsUp (interface))
    {
        return;
    }

    if (m_interfaceExclusions.find (interface) != m_interfaceExclusions.end ())
    {
        return;
    }

    Ipv4Address networkAddress = address.GetLocal ().CombineMask (address.GetMask ());
    Ipv4Mask networkMask = address.GetMask ();

    if (address.GetScope () == Ipv4InterfaceAddress::GLOBAL)
    {
        // Remove all previous routes that are going through this interface
        // which reference this network
        for (RoutesI it = m_routes.begin (); it != m_routes.end (); it++)
        {
            if (it->first->GetInterface () == interface
            && it->first->IsNetwork ()
            && it->first->GetDestNetwork () == networkAddress
            && it->first->GetDestNetworkMask () == networkMask)
            {
                InvalidateRoute (it->first);
            }
        }
        AddNetworkRouteTo (networkAddress, networkMask, interface);
    }

    SendTriggeredRouteUpdate ();
}

void Rip::NotifyRemoveAddress (uint32_t interface, Ipv4InterfaceAddress address)
{
    NS_LOG_FUNCTION (this << interface << address);

    if (!m_ipv4->IsUp (interface))
    {
        return;
    }

    if (address.GetScope() != Ipv4InterfaceAddress::GLOBAL)
    {
        return;
    }

    Ipv4Address networkAddress = address.GetLocal ().CombineMask (address.GetMask ());
    Ipv4Mask networkMask = address.GetMask ();

    // Remove all routes that are going through this interface
    // which reference this network
    for (RoutesI it = m_routes.begin (); it != m_routes.end (); it++)
    {
        if (it->first->GetInterface () == interface
        && it->first->IsNetwork ()
        && it->first->GetDestNetwork () == networkAddress
        && it->first->GetDestNetworkMask () == networkMask)
        {
            // Delete the entry from the distance table
            RipDistanceTableEntry dte;
            dte.SetNetwork(it->first->GetDestNetwork ());
            dte.SetNetworkMask(it->first->GetDestNetworkMask ());
            dte.SetRouteInterface(interface);
            m_distanceTable.DeleteDistanceTableEntry(&dte);

            std::cout << "\nNotify remove address invalidation\n\n";
            DeleteRoute (it->first);
            break;		  
        }
    }

    if (m_interfaceExclusions.find (interface) == m_interfaceExclusions.end ())
    {
        SendTriggeredRouteUpdate ();
    }

}

void Rip::SetIpv4 (Ptr<Ipv4> ipv4)
{
    NS_LOG_FUNCTION (this << ipv4);

    NS_ASSERT (m_ipv4 == 0 && ipv4 != 0);
    uint32_t i = 0;
    m_ipv4 = ipv4;

    for (i = 0; i < m_ipv4->GetNInterfaces (); i++)
    {
        if (m_ipv4->IsUp (i))
        {
            NotifyInterfaceUp (i);
        }
        else
        {
            NotifyInterfaceDown (i);
        }
    }
}


void Rip::DeletePrevRoutingTable(bool verbose)
{
    // Delete the previously tracked routing table. 
    if (verbose) printf("Deleting routing tables in prev_routes\n");
    for (RoutesI it = prev_routes.begin (); it != prev_routes.end (); it++)
    {
        delete it->first;
    }
    prev_routes.clear ();
}

void Rip::CopyRoutingTableEntry(RipRoutingTableEntry* copyRoute)
{
    // Copy the routing table entry.
    RipRoutingTableEntry* route = new RipRoutingTableEntry (copyRoute->GetDest (), copyRoute->GetDestNetworkMask (), copyRoute->GetInterface ());
    route->SetRouteMetric (copyRoute->GetRouteMetric());
    route->SetRouteStatus (copyRoute->GetRouteStatus());
    route->SetRouteChanged (copyRoute->IsRouteChanged());
    prev_routes.push_front (std::make_pair (route, EventId ()));
}

void Rip::CopyRoutingTableToPrev(bool verbose)
{
    // Copy the new routing table to the old one to be used for comparison. 
    if (verbose) printf("Copying routing table from m_routes to prev_routes.\n");
    for (RoutesCI currRoute = m_routes.begin (); currRoute != m_routes.end (); currRoute++)
    {
        CopyRoutingTableEntry(currRoute->first);
    }
}

bool Rip::CompareIndividualEntry(RipRoutingTableEntry* route1, RipRoutingTableEntry* route2, bool verbose)
{	
    // Get the metrics out of the routing tables.
    RipRoutingTableEntry::Status_e curr_status = route1->GetRouteStatus();
    Ipv4Address curr_dest = route1->GetDest ();
    Ipv4Mask curr_destMask = route1->GetDestNetworkMask (); 
    uint32_t curr_interface = route1->GetInterface ();  
    uint8_t curr_metric = route1->GetRouteMetric (); 
    bool curr_changed = route1->IsRouteChanged(); 
    if (verbose) printf("Read route1\n");

    RipRoutingTableEntry::Status_e prev_status = route2->GetRouteStatus();
    Ipv4Address prev_dest = route2->GetDest ();
    Ipv4Mask prev_destMask = route2->GetDestNetworkMask (); 
    uint32_t prev_interface = route2->GetInterface ();  
    uint8_t prev_metric = route2->GetRouteMetric (); 
    bool prev_changed = route2->IsRouteChanged(); 
    if (verbose) printf("Read route2\n");

    // Compare the old routing table with the new.
    if (!(curr_status == prev_status)) {return false;}	if (verbose) printf("Status good\n");
    if (!(curr_dest.IsEqual(prev_dest))) {return false;} if (verbose) printf("Dest good\n");
    if (!(curr_destMask.IsEqual(prev_destMask))) {return false;} if (verbose) printf("Mask good\n");
    if (!(curr_interface == prev_interface)) {return false;} if (verbose) printf("Outbound interface good\n");
    if (!(curr_metric == prev_metric)) {return false;} if (verbose) printf("Metric good\n");
    if (!(curr_changed == prev_changed)) {return false;} if (verbose) printf("Changed status good\n");

    if (verbose) printf("Leaving compare RTE\n");
    return true;
}

bool Rip::CompareRoutingTables(bool verbose)
{
    // First check if the previous roues are empty
    bool foundMatch = false;
    if ( !(prev_routes.empty()) )
    {

        // Check if the routing table routes are empty
        if (! m_routes.empty())
        {
            for (RoutesCI currRoute = m_routes.begin (); currRoute != m_routes.end (); currRoute++)
            {
                RipRoutingTableEntry* route = currRoute->first;

                foundMatch = false;
                for (RoutesCI  prevRoute = prev_routes.begin (); prevRoute != prev_routes.end (); prevRoute++)
                {	
                    if (CompareIndividualEntry(route, prevRoute->first, verbose))
                    {
                        if (verbose) printf("Routing table entries are the same!\n");
                        foundMatch = true;
                        break;
                    }
                }
                if (foundMatch == false)
                {
                    if (verbose) printf("Convergence has not yet been reached \n");
                    break;
                }
            }
            DeletePrevRoutingTable(verbose); // Delete routes in prev_routes
            CopyRoutingTableToPrev(verbose); // Copy routes from m_routes -> prev_routes
        }
        else
        {
            // If we got in here, that means that the routing tables and node have not been initialized yet.
            // We want to continue and wait for RTEs to be added.
            foundMatch = false;
        }
    }
    else
    {
        if (verbose) printf("Previous routes is empty.\n");
        CopyRoutingTableToPrev(verbose); // Copy routes from m_routes -> prev_routes
        foundMatch = false;
    }

    return foundMatch;
}

Time Rip::FindTimeOfConvergence(Ptr<OutputStreamWrapper> stream, Time::Unit unit, bool verbose)
{
    NS_LOG_FUNCTION ("Attempting to find convergence.");

    std::ostream* os = stream->GetStream ();

    // Reset the check for convergence.
    if (firstConvergenceCheck == false)
    {
        convergenceTimePassed = 0;
        convergenceStartTime = Seconds(Now().GetSeconds());
        firstConvergenceCheck = true;
        if (verbose) *os << "Start time: " << convergenceStartTime.As (unit) << ".\n";
        if (verbose) printf("Restarting check for convergence.\n");
        return Seconds(0);
    }

    // Debug message used to track how convergence is being found. 
    if (verbose) printf("%u: Convergence time passed: %u\n", m_ipv4->GetObject<Node> ()->GetId (), convergenceTimePassed);

    // Increment a counter each time the node gets a little closer to reaching the 'quiet time.'
    if (CompareRoutingTables(verbose))
    {
        convergenceTimePassed += 1;
    }
    else
    {
        // Reset the count for convergence when a routing table change occurs.
        firstConvergenceCheck = false;
        if (verbose) *os << "Start time: " << convergenceStartTime.As (unit) << ".\n";
    }

    // If the quiet time has been exceeded, mark convergence as being reached. 
    if (Seconds(convergenceTimePassed) >= QUIET_TIME)
    {
        *os << "Convergence was reached in Node: " << m_ipv4->GetObject<Node> ()->GetId ()
        << " at time " << convergenceStartTime.As (unit) << " It is now " << Now().As (unit) << ".\n";

        convergenceTimePassed = 0; //-20000;
        convergenceStartTime = Seconds(Now().GetSeconds());
        return convergenceStartTime;
    }	   

    return Seconds(0);
}

void Rip::EnableUseDistanceTables(bool use)
{
    m_useDT = use;
}

void Rip::PrintRoutingTable (Ptr<OutputStreamWrapper> stream, Time::Unit unit) const
{
    NS_LOG_FUNCTION (this << stream);

    std::ostream* os = stream->GetStream ();

    *os << "Node: " << m_ipv4->GetObject<Node> ()->GetId ()
    << ", Time: " << Now().As (unit)
    << ", Local time: " << GetObject<Node> ()->GetLocalTime ().As (unit)
    << ", IPv4 RIP table" << std::endl;

    if (!m_routes.empty ())
    {
        *os << "Destination     Gateway         Genmask         Status Metric RefDist Iface" << std::endl;
        for (RoutesCI it = m_routes.begin (); it != m_routes.end (); it++)
        {
            RipRoutingTableEntry* route = it->first;
            RipRoutingTableEntry::Status_e status = route->GetRouteStatus();

            if (route->GetDest ().IsEqual (Ipv4Address("0.0.0.0")))
            {
            continue;
            }

            if (1/*status == RipRoutingTableEntry::RIP_VALID*/)
            {
                std::ostringstream dest, gw, mask, flags;
                dest << route->GetDest ();
                *os << std::setiosflags (std::ios::left) << std::setw (16) << dest.str ();
                gw << route->GetGateway ();
                *os << std::setiosflags (std::ios::left) << std::setw (16) << gw.str ();
                mask << route->GetDestNetworkMask ();
                *os << std::setiosflags (std::ios::left) << std::setw (16) << mask.str ();
                flags << status;
                if (route->IsHost ())
                {
                    //flags << "HS";
                }
                else if (route->IsGateway ())
                {
                    //flags << "GS";
                }
                *os << std::setiosflags (std::ios::left) << std::setw (7) << flags.str ();
                *os << std::setiosflags (std::ios::left) << std::setw (7) << int(route->GetRouteMetric ());
                *os << std::setiosflags (std::ios::left) << std::setw (8) << int(route->GetReferenceDistance());

                if (Names::FindName (m_ipv4->GetNetDevice (route->GetInterface ())) != "")
                {
                    *os << Names::FindName (m_ipv4->GetNetDevice (route->GetInterface ()));
                }
                else
                {
                    *os << std::setiosflags (std::ios::left) << std::setw (6) << route->GetInterface ();
                }
                    *os << std::endl;
            }
        }
    }
    *os << std::endl;
}

void Rip::PrintDistanceTable (Ptr<OutputStreamWrapper> stream, Time::Unit unit)
{
    uint32_t nodeID = (uint32_t) m_ipv4->GetObject<Node> ()->GetId ();
    m_distanceTable.Print(stream, unit, nodeID);
}

void Rip::DoDispose ()
{
NS_LOG_FUNCTION (this);

    for (RoutesI j = m_routes.begin ();  j != m_routes.end (); j = m_routes.erase (j))
    {
        delete j->first;
    }
    m_routes.clear ();

    m_nextTriggeredUpdate.Cancel ();
    m_nextUnsolicitedUpdate.Cancel ();
    m_nextTriggeredUpdate = EventId ();
    m_nextUnsolicitedUpdate = EventId ();

    for (SocketListI iter = m_sendSocketList.begin (); iter != m_sendSocketList.end (); iter++ )
    {
        iter->first->Close ();
    }
    m_sendSocketList.clear ();

    m_recvSocket->Close ();
    m_recvSocket = 0;

    m_ipv4 = 0;

    Ipv4RoutingProtocol::DoDispose ();
}

Ptr<Ipv4Route> Rip::DistanceTableLookup (Ipv4Address dst, Ptr<NetDevice> interface)
{
    NS_LOG_FUNCTION (this << dst << interface);

    Ptr<Ipv4Route> rtentry = 0;

    /* when sending on local multicast, there have to be interface specified */
    if (dst.IsLocalMulticast ())
    {
        NS_ASSERT_MSG (interface, "Try to send on local multicast address, and no interface index is given!");
        rtentry = Create<Ipv4Route> ();
        rtentry->SetSource (m_ipv4->SourceAddressSelection (m_ipv4->GetInterfaceForDevice (interface), dst));
        rtentry->SetDestination (dst);
        rtentry->SetGateway (Ipv4Address::GetZero ());
        rtentry->SetOutputDevice (interface);
        return rtentry;
    }

    RipRoutingTableEntry *entry = m_distanceTable.GetDistanceTableRoute(dst);
    if (entry == NULL)
        return rtentry;

    Ipv4RoutingTableEntry* route = entry;
    uint32_t interfaceIdx = route->GetInterface ();
    rtentry = Create<Ipv4Route> ();

    if (route->GetDest ().IsAny ()) /* default route */
    {
        rtentry->SetSource (m_ipv4->SourceAddressSelection (interfaceIdx, route->GetGateway ()));
    }
    else
    {
        rtentry->SetSource (m_ipv4->SourceAddressSelection (interfaceIdx, route->GetDest ()));
    }

    rtentry->SetDestination (route->GetDest ());
    rtentry->SetGateway (route->GetGateway ());
    rtentry->SetOutputDevice (m_ipv4->GetNetDevice (interfaceIdx));

    if (rtentry)
    {
        NS_LOG_LOGIC ("Matching route via " << rtentry->GetDestination () << " (through " << rtentry->GetGateway () << ") at the end");
    }
    return rtentry;
}

Ptr<Ipv4Route> Rip::Lookup (Ipv4Address dst, Ptr<NetDevice> interface)
{
    NS_LOG_FUNCTION (this << dst << interface);

    Ptr<Ipv4Route> rtentry = 0;
    uint16_t longestMask = 0;

    /* when sending on local multicast, there have to be interface specified */
    if (dst.IsLocalMulticast ())
    {
        NS_ASSERT_MSG (interface, "Try to send on local multicast address, and no interface index is given!");
        rtentry = Create<Ipv4Route> ();
        rtentry->SetSource (m_ipv4->SourceAddressSelection (m_ipv4->GetInterfaceForDevice (interface), dst));
        rtentry->SetDestination (dst);
        rtentry->SetGateway (Ipv4Address::GetZero ());
        rtentry->SetOutputDevice (interface);
        return rtentry;
    }

    for (RoutesI it = m_routes.begin (); it != m_routes.end (); it++)
    {
        RipRoutingTableEntry* j = it->first;

        if (j->GetRouteStatus () == RipRoutingTableEntry::RIP_VALID)
        {
            Ipv4Mask mask = j->GetDestNetworkMask ();
            uint16_t maskLen = mask.GetPrefixLength ();
            Ipv4Address entry = j->GetDestNetwork ();

            NS_LOG_LOGIC ("Searching for route to " << dst << ", mask length " << maskLen);

            if (mask.IsMatch (dst, entry))
            {
                NS_LOG_LOGIC ("Found global network route " << j << ", mask length " << maskLen);

                /* if interface is given, check the route will output on this interface */
                if (!interface || interface == m_ipv4->GetNetDevice (j->GetInterface ()))
                {
                    if (maskLen < longestMask)
                    {
                        NS_LOG_LOGIC ("Previous match longer, skipping");
                        continue;
                    }

                    longestMask = maskLen;

                    Ipv4RoutingTableEntry* route = j;
                    uint32_t interfaceIdx = route->GetInterface ();
                    rtentry = Create<Ipv4Route> ();

                    if (route->GetDest ().IsAny ()) /* default route */
                    {
                        rtentry->SetSource (m_ipv4->SourceAddressSelection (interfaceIdx, route->GetGateway ()));
                    }
                    else
                    {
                        rtentry->SetSource (m_ipv4->SourceAddressSelection (interfaceIdx, route->GetDest ()));
                    }

                    rtentry->SetDestination (route->GetDest ());
                    rtentry->SetGateway (route->GetGateway ());
                    rtentry->SetOutputDevice (m_ipv4->GetNetDevice (interfaceIdx));
                }
            }
        }
    }

    if (rtentry)
    {
        NS_LOG_LOGIC ("Matching route via " << rtentry->GetDestination () << " (through " << rtentry->GetGateway () << ") at the end");
    }
    return rtentry;
}

void Rip::AddNetworkRouteTo (Ipv4Address network, Ipv4Mask networkPrefix, Ipv4Address nextHop, uint32_t interface)
{
    NS_LOG_FUNCTION (this << network << networkPrefix << nextHop << interface);

    RipRoutingTableEntry* route = new RipRoutingTableEntry (network, networkPrefix, nextHop, interface);
    route->SetRouteMetric (1);
    route->SetReferenceDistance(0); // Directly connected routes have a reference distance of 0 because they are the destination
    route->SetRouteStatus (RipRoutingTableEntry::RIP_VALID);
    route->SetRouteChanged (true);

    m_routes.push_back (std::make_pair (route, EventId ()));
}

void Rip::AddNetworkRouteTo (Ipv4Address network, Ipv4Mask networkPrefix, uint32_t interface)
{
    NS_LOG_FUNCTION (this << network << networkPrefix << interface);

    RipRoutingTableEntry* route = new RipRoutingTableEntry (network, networkPrefix, interface);
    route->SetRouteMetric (1);
    route->SetReferenceDistance(0); // Directly connected routes have a reference distance of 0 because they are the destination
    route->SetRouteStatus (RipRoutingTableEntry::RIP_VALID);
    route->SetRouteChanged (true);

    m_routes.push_back (std::make_pair (route, EventId ()));

    // Add a distance table entry as well
    RipDistanceTableEntry *dte = new RipDistanceTableEntry(network, networkPrefix, Ipv4Address("0.0.0.0"), interface, 1, 0);

    uint32_t nodeID = (uint32_t) m_ipv4->GetObject<Node> ()->GetId ();
    m_distanceTable.AddDistanceTableEntry(dte, nodeID);
}

void Rip::AllowPRTSend (RipPendingRequestEntry *route)
{
    for (PendingRequestsI it = m_pendingRequestTable.begin (); it != m_pendingRequestTable.end (); it++)
    {
        // Check if the incoming RTE is in the pending request table.
        if (it->first->GetNetwork () == route->GetNetwork () && it->first->GetNetworkMask () == route->GetNetworkMask ())
        {
            if (it->first->GetRouteInterface () == route->GetRouteInterface ())
            {
                // Check if the reference distance is able to satisfy the initial request.
                if (route->GetReferenceDistance () == it->first->GetReferenceDistance ())
                {
                    it->first->SetForwardTimer (false);
                    break;
                }
            }
        }	
    }
}

void Rip::InvalidateRoute (RipRoutingTableEntry *route)
{
    NS_LOG_FUNCTION (this << *route);

    for (RoutesI it = m_routes.begin (); it != m_routes.end (); it++)
    {
        if (it->first == route)
        {
            // If the route is invalid, we do not need to invalidate it again.
            if (route->GetRouteStatus () == RipRoutingTableEntry::RIP_INVALID)
            {
                std::cout << "Invalidating already invalid route\n";
                return;
            }

            route->SetRouteStatus (RipRoutingTableEntry::RIP_INVALID);
            uint32_t curMetric = route->GetRouteMetric ();
            route->SetRouteMetric (m_linkDown);
            route->SetReferenceDistance (curMetric - 1); // Set the reference distance to the previous route distance
            route->SetRouteChanged (true);
            if (it->second.IsRunning ())
            {
                it->second.Cancel ();
            }

            // We want to find a new valid route if this one was invalidated, except if the distance is 1 (this means we are directly connected and there
            // cannot be a better route out there. Must wait for link to come back up.
            if (curMetric == 1) return;		

            // We invalidated a route, meaning that we want to try to find this route again. Send out a request for this route.
            RipRte reqRTE;
            reqRTE.SetPrefix (route->GetDestNetwork ());
            // This needs to be done because ns3 does not support retrieving masks from ipv4 addresses.
            // This should not be done in practice, but all of my simulations use this network mask, so this is 
            // an easy hacky solution to make the simulations run.
            reqRTE.SetSubnetMask ((Ipv4Mask) "255.255.255.0");
            // Since we invalidate an old route, set reference distance to what it was previously.
            reqRTE.SetReferenceDistance (0);//curMetric - 1);
            
            std::cout << Now ().As(Time::S) << " Invalidated a route in node " << m_ipv4->GetObject<Node> ()->GetId () << " for destination " << route->GetDestNetwork () << " with RD: " << curMetric - 1 << "\n";

            bool noreq = false;
            if (m_useDT)
            {
                uint32_t validEntries = m_distanceTable.GetNumValidDTEntries(reqRTE.GetPrefix ());
                // std::cout << "Trying to swap out entries in node " << m_ipv4->GetObject<Node> ()->GetId () << " for destination " << route->GetDestNetwork () << " and there are valid entries: " << validEntries << "\n";
                if (validEntries > 1)
                {
                    // If we are using distance tables, then try to swap in a new route to the distance table immediately.
                    RipRoutingTableEntry* entry;
                    int count = 0; // Try a lot of times, if it doesn't work then don't want to be stuck here forever
                    while (count < 100)
                    {
                        entry = m_distanceTable.GetDistanceTableRoute (reqRTE.GetPrefix ());
                        if (entry && (entry->GetGateway () != route->GetGateway ()))
                        {
                            break;
                        }
                        count++;
                    }
                    if (entry)
                    {
                        DeleteRoute(route); // Delete previous route from RT, it is still in DT
                        std::cout << "Swapping out DT entry to RT for dest " << entry->GetDestNetwork () << " with gw " << entry->GetGateway () << " old gw " << route->GetGateway () << " with dist: " << entry->GetRouteMetric () << "\n"; 
                        m_routes.push_front (std::make_pair (entry, EventId ()));
                        EventId invalidateEvent = Simulator::Schedule (m_timeoutDelay, &Rip::InvalidateRoute, this, entry);
                        (m_routes.begin ())->second = invalidateEvent;
                        noreq = true;
                    }
                }			
            }

            if (!noreq)
            {
                SendIndividualRouteRequest(reqRTE, 0);
            }

            // Don't want to delete old routes, just invalidate them because it is easier to enable an old route. 
            // Storage is large nowadays, so we don't care if we store an extra ~24 bytes.
            // it->second = Simulator::Schedule (m_garbageCollectionDelay, &Rip::DeleteRoute, this, route);
            return;
        }
    }
    NS_ABORT_MSG ("RIP::InvalidateRoute - cannot find the route to update");
}

// This function no longer gets called because we dont want to delete old routes, just invalidate them
void Rip::DeleteRoute (RipRoutingTableEntry *route)
{
NS_LOG_FUNCTION (this << *route);

    for (RoutesI it = m_routes.begin (); it != m_routes.end (); it++)
    {
        if (it->first == route)
        {
            delete route;
            m_routes.erase (it);
            return;
        }
    }
    NS_ABORT_MSG ("RIP::DeleteRoute - cannot find the route to delete");
}

// Delete an entry from the pending request table
void Rip::DeletePRTEntry (RipPendingRequestEntry *entry)
{
    NS_LOG_FUNCTION (this << *entry);

    for (PendingRequestsI it = m_pendingRequestTable.begin (); it != m_pendingRequestTable.end (); it++)
    {
        if (it->first == entry)
        {
            delete entry;
            m_pendingRequestTable.erase (it);
            return;
        }
    }
    std::cout << "DeletePRTEntry <> Couldnt find entry to delete from PRT!!!\n";
}


void Rip::Receive (Ptr<Socket> socket)
{
    NS_LOG_FUNCTION (this << socket);

    Address sender;
    Ptr<Packet> packet = socket->RecvFrom (sender);
    InetSocketAddress senderAddr = InetSocketAddress::ConvertFrom (sender);
    NS_LOG_INFO ("Received " << *packet << " from " << senderAddr);

    Ipv4Address senderAddress = senderAddr.GetIpv4 ();
    uint16_t senderPort = senderAddr.GetPort ();

    Ipv4PacketInfoTag interfaceInfo;
    if (!packet->RemovePacketTag (interfaceInfo))
    {
        NS_ABORT_MSG ("No incoming interface on RIP message, aborting.");
    }
    uint32_t incomingIf = interfaceInfo.GetRecvIf ();
    Ptr<Node> node = this->GetObject<Node> ();
    Ptr<NetDevice> dev = node->GetDevice (incomingIf);
    uint32_t ipInterfaceIndex = m_ipv4->GetInterfaceForDevice (dev);

    SocketIpTtlTag hoplimitTag;
    if (!packet->RemovePacketTag (hoplimitTag))
    {
        NS_ABORT_MSG ("No incoming Hop Count on RIP message, aborting.");
    }
    uint8_t hopLimit = hoplimitTag.GetTtl ();

    int32_t interfaceForAddress = m_ipv4->GetInterfaceForAddress (senderAddress);
    if (interfaceForAddress != -1)
    {
        NS_LOG_LOGIC ("Ignoring a packet sent by myself.");
        return;
    }

    RipHeader hdr;
    packet->RemoveHeader (hdr);
    //std::cout << "Received header " << hdr << "\n";
    if (hdr.GetCommand () == RipHeader::RESPONSE)
    {
        HandleResponses (hdr, senderAddress, ipInterfaceIndex, hopLimit, true);
    }
    else if (hdr.GetCommand () == RipHeader::REQUEST)
    {
        HandleRequests (hdr, senderAddress, senderPort, ipInterfaceIndex, hopLimit);
    }
    else if (hdr.GetCommand () == RipHeader::REPLY)
    {
        HandleReplys (hdr, senderAddress, ipInterfaceIndex, hopLimit);
    }
    else
    {
        std::cout << "Unknown command " << hdr.GetCommand () << "\n";
        NS_LOG_LOGIC ("Ignoring message with unknown command: " << int (hdr.GetCommand ()));
    }
    return;
}

void Rip::HandleRequests (RipHeader requestHdr, Ipv4Address senderAddress, uint16_t senderPort, uint32_t incomingInterface, uint8_t hopLimit)
{
    NS_LOG_FUNCTION (this << senderAddress << int (senderPort) << incomingInterface << int (hopLimit) << requestHdr);

    UpdateStat(RECV_REQ, requestHdr.GetSerializedSize());

    if (m_interfaceExclusions.find (incomingInterface) != m_interfaceExclusions.end ())
    {
        NS_LOG_LOGIC ("Ignoring a request message from an excluded interface: " << incomingInterface);
        return;
    }

    std::list<RipRte> rtes = requestHdr.GetRteList ();

    if (rtes.empty ())
    {
        return;
    }

    // Current RIPv2 requests only occur when on initialization of RIP object (router). It always requests 
    // a single entry, which is to say the whole routing table. This might change in the future with reference distances. 

    // check if it's a request for the full table from a neighbor
    if (rtes.size () == 1)
    {
        // This check checks as noted above. If it is a single entry that has any address, then respond with the whole thing.
        // Reference distances wouldn't even be checked at this time. Another else statement needs to be added. 
        if (rtes.begin ()->GetPrefix () == Ipv4Address::GetAny () &&
            rtes.begin ()->GetSubnetMask ().GetPrefixLength () == 0 &&
            rtes.begin ()->GetRouteMetric () == m_linkDown)
        {
            // Output whole thing. Use Split Horizon
            if (m_interfaceExclusions.find (incomingInterface) == m_interfaceExclusions.end ())
            {
                // we use one of the sending sockets, as they're bound to the right interface
                // and the local address might be used on different interfaces.
                Ptr<Socket> sendingSocket;
                for (SocketListI iter = m_sendSocketList.begin (); iter != m_sendSocketList.end (); iter++ )
                {
                    if (iter->second == incomingInterface)
                    {
                        sendingSocket = iter->first;
                    }
                }
                NS_ASSERT_MSG (sendingSocket, "HandleRequest - Impossible to find a socket to send the reply");

                uint16_t mtu = m_ipv4->GetMtu (incomingInterface);
                uint16_t maxRte = (mtu - Ipv4Header ().GetSerializedSize () - UdpHeader ().GetSerializedSize () - RipHeader ().GetSerializedSize ()) / RipRte ().GetSerializedSize ();

                Ptr<Packet> p = Create<Packet> ();
                SocketIpTtlTag tag;
                p->RemovePacketTag (tag);
                if (senderAddress == Ipv4Address(RIP_ALL_NODE))
                {
                    tag.SetTtl (1);
                }
                else
                {
                    tag.SetTtl (255);
                }
                p->AddPacketTag (tag);

                RipHeader hdr;
                hdr.SetCommand (RipHeader::RESPONSE);

                for (RoutesI rtIter = m_routes.begin (); rtIter != m_routes.end (); rtIter++)
                {
                    bool splitHorizoning = (rtIter->first->GetInterface () == incomingInterface);

                    Ipv4InterfaceAddress rtDestAddr = Ipv4InterfaceAddress(rtIter->first->GetDestNetwork (), rtIter->first->GetDestNetworkMask ());

                    bool isGlobal = (rtDestAddr.GetScope () == Ipv4InterfaceAddress::GLOBAL);
                    bool isDefaultRoute = ((rtIter->first->GetDestNetwork () == Ipv4Address::GetAny ()) &&
                    (rtIter->first->GetDestNetworkMask () == Ipv4Mask::GetZero ()) &&
                    (rtIter->first->GetInterface () != incomingInterface));

                    if ((isGlobal || isDefaultRoute) &&
                        (rtIter->first->GetRouteStatus () == RipRoutingTableEntry::RIP_VALID) )
                    {
                        RipRte rte;
                        rte.SetPrefix (rtIter->first->GetDestNetwork ());
                        rte.SetSubnetMask (rtIter->first->GetDestNetworkMask ());
                        if (m_splitHorizonStrategy == POISON_REVERSE && splitHorizoning)
                        {
                            rte.SetRouteMetric (m_linkDown);
                        }
                        else
                        {
                            rte.SetRouteMetric (rtIter->first->GetRouteMetric ());
                        }
                        rte.SetRouteTag (rtIter->first->GetRouteTag ());


                        // Additional logic will eventually need to be added here to support reference distances.
                        // A new function will be created called SendResponseToRequest that will call this function,
                        // that will use a reference distance equal to reference distance and not route metric.
                        rte.SetReferenceDistance (rtIter->first->GetRouteMetric ());


                        if ((m_splitHorizonStrategy != SPLIT_HORIZON) ||
                            (m_splitHorizonStrategy == SPLIT_HORIZON && !splitHorizoning))
                        {
                            hdr.AddRte (rte);
                        }
                    }
                    if (hdr.GetRteNumber () == maxRte)
                    {
                        p->AddHeader (hdr);
                        NS_LOG_DEBUG ("SendTo: " << *p);
                        UpdateStat(SEND_RESP, hdr.GetSerializedSize());
                        sendingSocket->SendTo (p, 0, InetSocketAddress (senderAddress, RIP_PORT));
                        p->RemoveHeader (hdr);
                        hdr.ClearRtes ();
                    }
                }

                if (hdr.GetRteNumber () > 0)
                {
                    p->AddHeader (hdr);
                    NS_LOG_DEBUG ("SendTo: " << *p);
                    UpdateStat(SEND_RESP, hdr.GetSerializedSize());
                    sendingSocket->SendTo (p, 0, InetSocketAddress (senderAddress, RIP_PORT));
                }
            }
        }
        else
        {
            // Check an individual request for an individual RTE.			
            RipRte foundRTE;
            for (std::list<RipRte>::iterator iter = rtes.begin (); iter != rtes.end (); iter++)
            {
                Ipv4Address requestedAddress = iter->GetPrefix ();
                Ipv4Address combinedRTEAddr = requestedAddress.CombineMask (iter->GetSubnetMask ());

                // if (combinedRTEAddr == Ipv4Address("10.0.11.0"))
                   // std::cout << "\n\n\n" << Now ().As(Time::S) << " Got request at node " << m_ipv4->GetObject<Node> ()->GetId ()  << " for dest: " << combinedRTEAddr << "\n";

                // Track if we found a route to the requested destination
                RipRoutingTableEntry *rd_too_high = NULL;
                // Search through the local routing table for the destination
                bool found = false;
                for (RoutesI rtIter = m_routes.begin (); rtIter != m_routes.end (); rtIter++)
                {
                    // First check if the address type matches
                    Ipv4InterfaceAddress rtDestAddr = Ipv4InterfaceAddress (rtIter->first->GetDestNetwork (), rtIter->first->GetDestNetworkMask ());
                    if ((rtDestAddr.GetScope () == Ipv4InterfaceAddress::GLOBAL) &&
                        (rtIter->first->GetRouteStatus () == RipRoutingTableEntry::RIP_VALID))
                    {

                        Ipv4Address rtAddress = rtIter->first->GetDestNetwork ();
                        Ipv4Address combinedAddr = rtAddress.CombineMask (rtIter->first->GetDestNetworkMask ());

                        // Check if the destination addresses match
                        if (combinedAddr == combinedRTEAddr)
                        {
                            // Check if this node's reference distance is less than the incoming request.
                            // If it is, then we can reply to the request.
                            if ((uint32_t) (rtIter->first->GetReferenceDistance ()) < iter->GetReferenceDistance ())
                            {	
                                foundRTE.SetPrefix (rtIter->first->GetDestNetwork ());
                                foundRTE.SetSubnetMask (rtIter->first->GetDestNetworkMask ());
                                foundRTE.SetReferenceDistance (rtIter->first->GetReferenceDistance () + 1);
                                foundRTE.SetRouteMetric (rtIter->first->GetRouteMetric ());
                                foundRTE.SetRouteTag (rtIter->first->GetRouteTag ());

                                found = true;
                                break;
                            }
                            else if (iter->GetReferenceDistance () == 0 && rtIter->first->GetReferenceDistance () == 0)
                            {
                                foundRTE.SetPrefix (rtIter->first->GetDestNetwork ());
                                foundRTE.SetSubnetMask (rtIter->first->GetDestNetworkMask ());
                                foundRTE.SetReferenceDistance (rtIter->first->GetReferenceDistance ());
                                foundRTE.SetRouteMetric (rtIter->first->GetRouteMetric ());
                                foundRTE.SetRouteTag (rtIter->first->GetRouteTag ());
                                
                                // if (combinedRTEAddr == Ipv4Address("10.0.11.0"))
                                    // std::cout << Now().As(Time::S) << "Able to satisfy request for " << combinedRTEAddr << " with RD = 0\n";
                                
                                found = true;
                                break;
                            }
                            else
                            {
                                // If we found a route to the destination, but not the proper RD, 
                                // then only forward request through that route
                                if (rtIter->first->GetRouteMetric () != m_linkDown)
                                {
                                    rd_too_high = rtIter->first;
                                }
                            }
                        }
                    }
                }
                if (found)
                {
                    // Found an entry that can satisfy the request, send a response back.
                    NS_LOG_LOGIC ("Responded to individual packet request at node: " <<  m_ipv4->GetObject<Node> ()->GetId () << " from interface " << int (incomingInterface) << " at " << Now().As (Time::S) << ". \n");

                    // We only want to respond to the node that requested this entry if we can satisfy this request.
                    Ptr<Socket> sendingSocket;
                    for (SocketListI iter = m_sendSocketList.begin (); iter != m_sendSocketList.end (); iter++ )
                    {
                        if (iter->second == incomingInterface)
                        {
                            sendingSocket = iter->first;
                        }
                    }
                    NS_ASSERT_MSG (sendingSocket, "HandleRequest - Impossible to find a socket to send the reply");

                    // Send an immediate reply to the requestor
                    DoSendRouteReply(foundRTE, sendingSocket, senderAddress, true);
                }
                else
                {
                    // Forward request out all ports except for incoming port if the destination could not be found in our routin table.
                    NS_LOG_LOGIC ("Forwarded individual packet request at node: " <<  m_ipv4->GetObject<Node> ()->GetId () << " from interface " << int (incomingInterface)  << " at " << Now().As (Time::S) << ". \n");

                    if (hopLimit <= 1) return; // If the TTL is at 1 or lower and the request couldnt be met, simply drop the packet					

                    // Check if the entry already exists in the PRT
                    bool looking = false;
                    bool recentForward = false;
                    PendingRequestsI it;
                    for (it = m_pendingRequestTable.begin (); it != m_pendingRequestTable.end (); it++)
                    {
                        // Check if the incoming RTE is in the pending request table.
                        if (it->first->GetNetwork () == combinedRTEAddr && it->first->GetNetworkMask () == iter->GetSubnetMask ())
                        {
                            if (it->first->GetRouteInterface () == incomingInterface)
                            {
                                // Check if the reference distance is able to satisfy the initial request.
                                if (iter->GetReferenceDistance () == it->first->GetReferenceDistance ())
                                {
                                    looking = true;
                                    recentForward = it->first->GetForwardTimer ();
                                    break;
                                }
                            }
                        }	
                    }

                    if ( ! looking )
                    {
                        // Add an entry into the Pending Request Table for this request.
                        NS_LOG_LOGIC ("Received a request that cannot be satisfied, adding to PRT.");
                        
                        RipPendingRequestEntry* new_prt_entry = new RipPendingRequestEntry ();
                        new_prt_entry->SetNetwork (combinedRTEAddr);
                        new_prt_entry->SetNetworkMask (iter->GetSubnetMask ());
                        new_prt_entry->SetReferenceDistance (iter->GetReferenceDistance ());
                        new_prt_entry->SetRouteInterface (incomingInterface);
                        new_prt_entry->SetForwardTimer (true);

                        m_pendingRequestTable.push_front (std::make_pair (new_prt_entry, EventId ()));
                        EventId allowRequest = Simulator::Schedule (m_forwardingDelay, &Rip::AllowPRTSend, this, new_prt_entry);
                        (m_pendingRequestTable.begin ())->second = allowRequest;
                    }
                    else
                    {
                        if (recentForward)
                        {
                            continue;
                        }
                        it->first->SetForwardTimer (true);
                        EventId allowRequest = Simulator::Schedule (m_forwardingDelay, &Rip::AllowPRTSend, this, it->first);
                        it->second = allowRequest;
                    }

                    // Create a packet to forward the request.
                    Ptr<Packet> forwardPack = Create<Packet> ();
                    SocketIpTtlTag tag;
                    forwardPack->RemovePacketTag (tag);
                    tag.SetTtl (hopLimit - 1); // Set the time to live as the previous TTL - 1
                    forwardPack->AddPacketTag (tag);

                    RipHeader req_hdr;
                    req_hdr.SetCommand (RipHeader::REQUEST);
                    req_hdr.AddRte (*iter);
                    forwardPack->AddHeader (req_hdr);

                    NS_LOG_LOGIC(Now ().As(Time::S) << " Forwarding request at " << m_ipv4->GetObject<Node> ()->GetId () << " with " << combinedRTEAddr << " " << iter->GetReferenceDistance () << " " << incomingInterface << "\n");

                    Ptr<Socket> forwardSocket;
                    if (rd_too_high)
                    {
                        // std::cout << Now ().As(Time::S) << "Found secondary route\n";
                        // std::cout << rd_too_high->GetDestNetwork () << "\n";
                        for (SocketListI iter = m_sendSocketList.begin (); iter != m_sendSocketList.end (); iter++ )
                        {
                            if (iter->second == rd_too_high->GetInterface ())
                            {
                                UpdateStat(SEND_REQ, req_hdr.GetSerializedSize()); // Do not want to track forwarded requests
                                // std::cout << Now ().As(Time::S) << "Found secondary route in node " << m_ipv4->GetObject<Node> ()->GetId () << " for route " << rd_too_high->GetDestNetwork () << " out iface " << rd_too_high->GetInterface () << "\n";
                                forwardSocket = iter->first;
                                forwardSocket->SendTo (forwardPack, 0, InetSocketAddress (RIP_ALL_NODE, RIP_PORT));
                                break;
                            }
                        }
                    }
                    else
                    {
                        // Forward the request out of each interface except the one it came in on
                        for (SocketListI iter = m_sendSocketList.begin (); iter != m_sendSocketList.end (); iter++ )
                        {
                            if (iter->second != incomingInterface)
                            {
                                UpdateStat(SEND_REQ, req_hdr.GetSerializedSize()); // Do not want to track forwarded requests
                                forwardSocket = iter->first;
                                forwardSocket->SendTo (forwardPack, 0, InetSocketAddress (RIP_ALL_NODE, RIP_PORT));
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        // note: we got the request as a single packet, so no check is necessary for MTU limit

        Ptr<Packet> p = Create<Packet> ();
        SocketIpTtlTag tag;
        p->RemovePacketTag (tag);
        if (senderAddress == Ipv4Address(RIP_ALL_NODE))
        {
            tag.SetTtl (1);
        }
        else
        {
            tag.SetTtl (255);
        }
        p->AddPacketTag (tag);

        RipHeader hdr;
        hdr.SetCommand (RipHeader::RESPONSE);

        for (std::list<RipRte>::iterator iter = rtes.begin (); iter != rtes.end (); iter++)
        {
            Ipv4Address requestedAddress = iter->GetPrefix ();
            requestedAddress.CombineMask (iter->GetSubnetMask ());

            bool found = false;
            for (RoutesI rtIter = m_routes.begin (); rtIter != m_routes.end (); rtIter++)
            {
                Ipv4InterfaceAddress rtDestAddr = Ipv4InterfaceAddress (rtIter->first->GetDestNetwork (), rtIter->first->GetDestNetworkMask ());
                if ((rtDestAddr.GetScope () == Ipv4InterfaceAddress::GLOBAL) &&
                    (rtIter->first->GetRouteStatus () == RipRoutingTableEntry::RIP_VALID))
                {

                    Ipv4Address rtAddress = rtIter->first->GetDestNetwork ();
                    rtAddress.CombineMask (rtIter->first->GetDestNetworkMask ());

                    if (requestedAddress == rtAddress)
                    {
                        // Check if this node's reference distance is less than the incoming request.
                        // If it is, then we can respond to the request.
                        if (rtIter->first->GetReferenceDistance () < iter->GetReferenceDistance ())
                        {
                            // A check will eventually have to be made here to check if incoming reference distance is greater 
                            // than the reference distance at this node. The propoer reference distance should also be given.
                            iter->SetReferenceDistance (rtIter->first->GetRouteMetric ());
                            iter->SetRouteMetric (rtIter->first->GetRouteMetric ());
                            iter->SetRouteTag (rtIter->first->GetRouteTag ());
                            hdr.AddRte (*iter);
                            found = true;
                            break;
                        }
                    }
                }
            }

            if (!found)
            {
                iter->SetRouteMetric (m_linkDown);
                iter->SetRouteTag (0);
                hdr.AddRte (*iter);
            }
        }
        p->AddHeader (hdr);
        NS_LOG_DEBUG ("SendTo: " << *p);
        UpdateStat(SEND_RESP, hdr.GetSerializedSize());
        m_recvSocket->SendTo (p, 0, InetSocketAddress (senderAddress, senderPort));
    }

}

void Rip::HandleResponses (RipHeader hdr, Ipv4Address senderAddress, uint32_t incomingInterface, uint8_t hopLimit, bool type)
{
    // The input type specifies if this message should be interpreted as a reply or a response. false is reply, and true is response


    NS_LOG_FUNCTION (this << senderAddress << incomingInterface << int (hopLimit) << hdr);

    if (m_interfaceExclusions.find (incomingInterface) != m_interfaceExclusions.end ())
    {
        NS_LOG_LOGIC ("Ignoring an update message from an excluded interface: " << incomingInterface);
        return;
    }

    std::list<RipRte> rtes = hdr.GetRteList ();

    if (rtes.size () == 1)
    {
        if (rtes.begin ()->GetPrefix () == Ipv4Address::GetAny () &&
            rtes.begin ()->GetSubnetMask ().GetPrefixLength () == 0 &&
            rtes.begin ()->GetRouteMetric () == m_linkDown)
        {
            for (RoutesI it = m_routes.begin (); it != m_routes.end (); it++)
            {
                if (incomingInterface == it->first->GetInterface () && 1 != it->first->GetRouteMetric ())
                {
                    it->second.Cancel ();
                    it->second = Simulator::Schedule (m_timeoutDelay, &Rip::InvalidateRoute, this, it->first);
                }
            }
            return;
        }
    }

    uint32_t nodeID = (uint32_t) m_ipv4->GetObject<Node> ()->GetId ();
    /* 	if (m_ipv4->GetObject<Node> ()->GetId () == 7)
    {
    std::cout << hdr.GetCommand() << " " << Now().As(Time::S) << " from interface " << incomingInterface << ":   \n";
    for (std::list<RipRte>::iterator iter = rtes.begin (); iter != rtes.end (); iter++)
    {
    iter->Print(std::cout);
    std::cout << "\n";
    }
    } */


    // validate the RTEs before processing
    for (std::list<RipRte>::iterator iter = rtes.begin (); iter != rtes.end (); iter++)
    {
        // Check that the routing metric and reference distance is not malformed
        if (iter->GetRouteMetric () == 0 || iter->GetRouteMetric () > m_linkDown || iter->GetReferenceDistance () > m_linkDown)
        {
            NS_LOG_LOGIC ("Ignoring an update message with malformed metric: " << int (iter->GetRouteMetric ()));
            return;
        }
        if (iter->GetPrefix ().IsLocalhost () ||
            iter->GetPrefix ().IsBroadcast () ||
            iter->GetPrefix ().IsMulticast ())
        {
            NS_LOG_LOGIC ("Ignoring an update message with wrong prefixes: " << iter->GetPrefix ());
            return;
        }
    }

    UpdateStat(RECV_RESP, hdr.GetSerializedSize());	

    bool changed = false;

    for (std::list<RipRte>::iterator iter = rtes.begin (); iter != rtes.end (); iter++)
    {
        Ipv4Mask rtePrefixMask = iter->GetSubnetMask ();
        Ipv4Address rteAddr = iter->GetPrefix ().CombineMask (rtePrefixMask);

        // Important: Reference Distance (RD) logic.
        // Get the reference distance from the RTE. 
        // For periodic updates, response messages contain a RD = D for a given destination.
        // Only for respones to requests will RD sent be equal to RD of node responding for that destination.
        // Triggered updates (not from requests) will behave the same as periodic updates.
        // This means that RD received can be put straight into new RTE for this routing table. 

        // Get reference distance from response RTE. If its a reply then we want to use the reference distance. If its 
        // a response then we want to use the metric - 1
        uint32_t responseReferenceDistance;
        if (type == false) // reply type
        {
            responseReferenceDistance = iter->GetReferenceDistance ();
        }
        else if (type == true) // response type
        {
            responseReferenceDistance =  iter->GetRouteMetric ();
        }

        NS_LOG_LOGIC ("Processing RTE " << *iter);

        uint32_t interfaceMetric = 1;
        // Check if the interface metric should not be using hop count (metric = 1)
        if (m_interfaceMetrics.find (incomingInterface) != m_interfaceMetrics.end ())
        {
            interfaceMetric = m_interfaceMetrics[incomingInterface];
            // If hop count is simulated to be something more than 1, account for that in the reference distance.
            responseReferenceDistance += interfaceMetric - 1;
        }

        // Get the RTE metric and add the interface metric just calculated. Check if it is infinity (>15)
        uint64_t rteMetric = iter->GetRouteMetric () + interfaceMetric;
        if (rteMetric > m_linkDown)
        {
            rteMetric = m_linkDown;
        }

        uint32_t newRD  = rteMetric - 1;


        RoutesI it;
        Ipv4Address gw = Ipv4Address(senderAddress);
        bool curChanged = false;
        bool found = false;
        // Loop through all of the routes in the router's routing table
        for (it = m_routes.begin (); it != m_routes.end (); it++)
        {
            // Check if the incoming RTE is in the routing table.
            if (it->first->GetDestNetwork () == rteAddr && it->first->GetDestNetworkMask () == rtePrefixMask)
            {
                found = true; // Found an entry!

                bool first = true;
                // Make this check because a bug occurs where directly connected routes do not get found properly
                // If this causes problems, it can be taken out and addnetworkinterface can change to use push_front instead of push_back
                for (RoutesI checkAgain = it; checkAgain != m_routes.end (); checkAgain++)
                {
                    if (first) {first = false; continue;}
                    if (checkAgain->first->GetDestNetwork () == rteAddr && checkAgain->first->GetDestNetworkMask () == rtePrefixMask)
                    {
                        uint8_t tempRD = checkAgain->first->GetReferenceDistance ();
                        if ((tempRD < it->first->GetReferenceDistance ()) && (tempRD < responseReferenceDistance))
                        {
                            found = false;
                        }
                    }
                }
                if (found == false)
                {
                    found = true;
                    break;
                }

                // If the gateway is the same and the metric is now 'unreachable', want to invalidate the route
                if (incomingInterface == it->first->GetInterface ()) //senderAddress == it->first->GetGateway ())
                {
                    if (rteMetric >= m_linkDown)
                    {
                        //std::cout << "Invalidating route at node " <<  m_ipv4->GetObject<Node> ()->GetId () << " with address " << rteAddr << " from interface " << incomingInterface << " at time " << Now().As(Time::S) << ". \n";

                        NS_LOG_LOGIC ("Invalidating route at node " <<  m_ipv4->GetObject<Node> ()->GetId () << " with metric equal to " << rteMetric <<
                        " at time " << Now().As(Time::S) << ". \n");

                        if (it->first->GetRouteStatus () == RipRoutingTableEntry::RIP_VALID)
                        {
                            std::cout << "Invalidating in node " << m_ipv4->GetObject<Node> ()->GetId () << " for destination " << it->first->GetDestNetwork () << " because got metric = " << rteMetric << " from iface " << incomingInterface << "\n";
                            InvalidateRoute (it->first);
                        }
                        changed = true;
                        curChanged = true;
                        break;
                    }
                }

                if (curChanged == false)
                {
                    // First check the reference distance. Don't trust a RTE that has a higher RD. We can still add it to the distance table for reference though.
                    if (responseReferenceDistance > it->first->GetReferenceDistance ())
                    {
                        RipDistanceTableEntry *dte = new RipDistanceTableEntry(rteAddr, rtePrefixMask, gw, incomingInterface, rteMetric, newRD);

                        m_distanceTable.AddDistanceTableEntry(dte, nodeID);
                        //if (nodeID == 2) m_distanceTable.Print(Create<OutputStreamWrapper> (&std::cout), Time::S, nodeID);

                        //if (nodeID == 7) std::cout << "Got higher RD in node " << m_ipv4->GetObject<Node> ()->GetId () << " for destination " << it->first->GetDestNetwork () << " and got metric = " << rteMetric << "\n";
                        // Refresh the invalidate timer
                        if (incomingInterface == it->first->GetInterface ()) //(senderAddress  == it->first->GetGateway () && rteMetric ==  it->first->GetRouteMetric ())
                        {
                            // Update a route that came through the same interface
                            if (rteMetric >  it->first->GetRouteMetric ())
                            {
                                RipRoutingTableEntry* route = new RipRoutingTableEntry (rteAddr, rtePrefixMask, senderAddress, incomingInterface);
                                route->SetReferenceDistance (newRD); // Set the reference distance
                                route->SetRouteMetric (rteMetric);
                                route->SetRouteStatus (RipRoutingTableEntry::RIP_VALID);
                                route->SetRouteTag (iter->GetRouteTag ());
                                route->SetRouteChanged (true);
                                delete it->first;
                                it->first = route;
                            }
                            it->second.Cancel ();
                            it->second = Simulator::Schedule (m_timeoutDelay, &Rip::InvalidateRoute, this, it->first);
                        }
                        continue;
                    }

                    // If the new metric is smaller than the current one, replace the current one.
                    if (rteMetric < it->first->GetRouteMetric ())
                    {
                        // If the gateway is different, then update the gateway
                        if (senderAddress != it->first->GetGateway ())
                        {
                            RipRoutingTableEntry* route = new RipRoutingTableEntry (rteAddr, rtePrefixMask, senderAddress, incomingInterface);
                            delete it->first;
                            it->first = route;
                        }
                        it->first->SetReferenceDistance (newRD); // Set the reference distance
                        it->first->SetRouteMetric (rteMetric);
                        it->first->SetRouteStatus (RipRoutingTableEntry::RIP_VALID);
                        it->first->SetRouteTag (iter->GetRouteTag ());
                        it->first->SetRouteChanged (true);
                        it->second.Cancel ();
                        it->second = Simulator::Schedule (m_timeoutDelay, &Rip::InvalidateRoute, this, it->first);
                        changed = true;
                        curChanged = true;
                    }
                    // If the new metric is the same as the current metric
                    else if (rteMetric == it->first->GetRouteMetric ())
                    {
                        // If the gateway is the same, update the timeout for the entry
                        if (senderAddress == it->first->GetGateway ())
                        {
                            it->second.Cancel ();
                            it->second = Simulator::Schedule (m_timeoutDelay, &Rip::InvalidateRoute, this, it->first);
                        }
                        // If the gateway is new and our current RTE hasn't been refreshed in awhile, update with the new RTE
                        else
                        {
                            if (Simulator::GetDelayLeft (it->second) < m_timeoutDelay/2)
                            {
                                RipRoutingTableEntry* route = new RipRoutingTableEntry (rteAddr, rtePrefixMask, senderAddress, incomingInterface);
                                route->SetReferenceDistance (newRD); // Set the reference distance
                                route->SetRouteMetric (rteMetric);
                                route->SetRouteStatus (RipRoutingTableEntry::RIP_VALID);
                                route->SetRouteTag (iter->GetRouteTag ());
                                route->SetRouteChanged (true);
                                delete it->first;
                                it->first = route;
                                it->second.Cancel ();
                                it->second = Simulator::Schedule (m_timeoutDelay, &Rip::InvalidateRoute, this, route);
                                changed = true;
                                curChanged = true;
                            }
                            else
                            {
                                curChanged = true; // Make sure this route gets added into the distance table, even though it has a different gateway
                            }
                        }
                    }
                    // If the new metric came from the current gateway but now has a higher metric, update. 
                    else if (rteMetric > it->first->GetRouteMetric () && senderAddress == it->first->GetGateway ())
                    {
                        if (hdr.GetCommand () == RipHeader::RESPONSE) // Do not want to accpet replys through the same interface because they might be older - potential loop issue
                        {
                            it->second.Cancel ();
                            if (rteMetric < m_linkDown)
                            {
                                it->first->SetReferenceDistance (newRD); // Set the reference distance
                                it->first->SetRouteMetric (rteMetric);
                                it->first->SetRouteStatus (RipRoutingTableEntry::RIP_VALID);
                                it->first->SetRouteTag (iter->GetRouteTag ());
                                it->first->SetRouteChanged (true);
                                it->second.Cancel ();
                                it->second = Simulator::Schedule (m_timeoutDelay, &Rip::InvalidateRoute, this, it->first);
                            }
                            else
                            {
                                std::cout << "Inv in node " << m_ipv4->GetObject<Node> ()->GetId () << " for destination " << it->first->GetDestNetwork () << " because got metric = " << rteMetric << "\n";
                                InvalidateRoute (it->first);
                            }
                            changed = true;
                            curChanged = true;
                        }
                    }
                }
            }
        }

        // If the RTE was not found in the current routing table, add it.
        if (!found && rteMetric != m_linkDown)
        {
            NS_LOG_LOGIC ("Received a RTE with new route, adding.");

            RipRoutingTableEntry* route = new RipRoutingTableEntry (rteAddr, rtePrefixMask, senderAddress, incomingInterface);
            route->SetReferenceDistance (newRD); // Set the reference distance
            route->SetRouteMetric (rteMetric);
            route->SetRouteStatus (RipRoutingTableEntry::RIP_VALID);
            route->SetRouteChanged (true);
            m_routes.push_front (std::make_pair (route, EventId ()));
            EventId invalidateEvent = Simulator::Schedule (m_timeoutDelay, &Rip::InvalidateRoute, this, route);
            (m_routes.begin ())->second = invalidateEvent;
            changed = true;
            curChanged = true;
        }

        if (curChanged)
        {
            RipDistanceTableEntry *dte = new RipDistanceTableEntry(rteAddr, rtePrefixMask, gw, incomingInterface, rteMetric, newRD);

            m_distanceTable.AddDistanceTableEntry(dte, nodeID);
            //if (nodeID == 2) m_distanceTable.Print(Create<OutputStreamWrapper> (&std::cout), Time::S, nodeID);
        }
    }

    if (changed)
    {
        SendTriggeredRouteUpdate ();
    }
}

void Rip::HandleReplys (RipHeader hdr, Ipv4Address senderAddress, uint32_t incomingInterface, uint8_t hopLimit)
{
    NS_LOG_FUNCTION (this << senderAddress << incomingInterface << int (hopLimit) << hdr);

    if (m_interfaceExclusions.find (incomingInterface) != m_interfaceExclusions.end ())
    {
        NS_LOG_LOGIC ("Ignoring an update message from an excluded interface: " << incomingInterface);
        return;
    }

    // Check if the reply has an outstanding request in the Pending Request Table
    std::list<RipRte> rtes = hdr.GetRteList ();
    
    // Check if the reply contains a metric that is too large. This can be done like this because replies only contain one entry.
    // We want to avoid the processing of this RTE so that it does not change the RT
    if (rtes.begin ()->GetRouteMetric () >= m_linkDown)
    {
        return;
    }

    // if (m_ipv4->GetObject<Node> ()->GetId () == 5 && Now () > Seconds (40))
    // {
        // std::cout << Now ().As (Time::S) << " Received reply in node "<< m_ipv4->GetObject<Node> ()->GetId ()  << " " << rtes.begin ()->GetPrefix () << " " << incomingInterface << " "<< 
                        // rtes.begin ()->GetReferenceDistance () << " " << rtes.begin ()->GetRouteMetric () <<"\n"; 
    // }

    // Skip the step of checking the PRT if it is empty
    if (m_pendingRequestTable.empty () == true)
    {
        // std::cout << "Skip reply if PRT is empty\n";
        return;
    }

    for (std::list<RipRte>::iterator iter = rtes.begin (); iter != rtes.end (); iter++)
    {
        // Get the destination address contained in the reply message
        Ipv4Mask rtePrefixMask = iter->GetSubnetMask ();
        Ipv4Address rteAddr = iter->GetPrefix ().CombineMask (rtePrefixMask);

        //std::cout << Now ().As(Time::S) << "\n";
        //std::cout << "Looking for entry in PRT in node " << m_ipv4->GetObject<Node> ()->GetId () << " at time " << Now ().As(Time::S) << " with " << iter->GetPrefix () << " " << iter->GetSubnetMask () << " " << iter->GetReferenceDistance () << "\n";

        bool handled = false;
        std::list <RipPendingRequestEntry *> entriesToDelete;
        // Search through the Pending Request Table to find a matching entry. If an entry is found that
        // matches the same network address, then check if the reference distance satisfies the request.
        // If all of these conditions are met, then forward the reply out of the interface associated with
        // the request. 
        for (PendingRequestsI it = m_pendingRequestTable.begin (); it != m_pendingRequestTable.end (); it++)
        {
            // Check if the incoming RTE is in the pending request table.
            if (it->first->GetNetwork () == rteAddr && it->first->GetNetworkMask () == rtePrefixMask)
            {
                // Check if the reference distance is able to satisfy the initial request.
                if (iter->GetReferenceDistance () <= it->first->GetReferenceDistance ())
                {
                    // Add entries to be deleted to a list
                    entriesToDelete.push_front (it->first);
                    
                    // Simply call the HandleResponse function because replys should be handled the same.
                    // The difference are that these reply messages will also be forwarded around the network 
                    // based on the pending request table in RIP.
                    if (handled == false)
                    {
                        // if (rteAddr == Ipv4Address("10.0.11.0") ) {
                            // std::cout << "Adding reply to routing  table at node " << m_ipv4->GetObject<Node> ()->GetId ()  << "\n";
                        // }
                        
                        HandleResponses (hdr, senderAddress, incomingInterface, hopLimit, false);
                        handled = true;
                    }         
                    
                    // We do not want to forward a request with a metric that is too large or one that is destined for this node 
                    if (iter->GetRouteMetric () >= m_linkDown - 1 || it->first->GetRouteInterface () == 0)
                    {
                        // std::cout << "PRT entry with interface 0 found\n";
                        continue;
                    }

                    // Forward the reply out of the interface that initially requested the route
                    Ptr<Socket> sendingSocket;
                    for (SocketListI sockIter = m_sendSocketList.begin (); sockIter != m_sendSocketList.end (); sockIter++ )
                    {
                        if (sockIter->second == it->first->GetRouteInterface ())
                        {
                            sendingSocket = sockIter->first;
                        }
                    }
                    NS_ASSERT_MSG (sendingSocket, "HandleReply - Impossible to find a socket to send the reply");

                    // Forward a new reply to be sent to the requestor that contains distance + 1, but ref dist is unchanged
                    RipRte foundRTE;
                    foundRTE.SetPrefix (iter->GetPrefix ());
                    foundRTE.SetSubnetMask (rtePrefixMask);
                    foundRTE.SetReferenceDistance (iter->GetReferenceDistance ()); // Set the reference distance as the same
                    foundRTE.SetRouteMetric (iter->GetRouteMetric () + 1);
                    foundRTE.SetRouteTag (iter->GetRouteTag ());

                    // Send an immediate reply to the requestor
                    DoSendRouteReply (foundRTE, sendingSocket, senderAddress, false);
                    // std::cout << "Forward route reply  in node " << m_ipv4->GetObject<Node> ()->GetId ()  << " for address " << rteAddr << " out iface " << it->first->GetRouteInterface () << "\n";
                }
            }
        }

        for (RipPendingRequestEntry *delIter = entriesToDelete.front (); entriesToDelete.empty () != true ; delIter = entriesToDelete.front ())
        {
            // Since the route was found, delete this route request from the pending request table
            DeletePRTEntry (delIter);
            entriesToDelete.pop_front ();
        }
    }

}

void Rip::DoSendRouteReply (RipRte rtes, Ptr<Socket> sendSock, Ipv4Address senderAddress, bool track)
{
    NS_LOG_FUNCTION (this);

    Ptr<Packet> p = Create<Packet> ();
    SocketIpTtlTag tag;
    p->RemovePacketTag (tag);
    tag.SetTtl (m_linkDown - 1); // Send the reply up to the maximum RIP distance
    p->AddPacketTag (tag);

    RipHeader hdr;
    hdr.SetCommand (RipHeader::REPLY);

    hdr.AddRte (rtes);
    p->AddHeader (hdr);

    // Unicast the reply only out of the requesting interface
    NS_LOG_DEBUG ("SendTo: " << *p);
    if (true) UpdateStat(SEND_RESP, hdr.GetSerializedSize()); // Do not want to track forwarded replies
    sendSock->SendTo (p, 0, InetSocketAddress (RIP_ALL_NODE, RIP_PORT));

}

void Rip::DoSendRouteUpdate (bool periodic)
{
    NS_LOG_FUNCTION (this << (periodic ? " periodic" : " triggered"));

    for (SocketListI iter = m_sendSocketList.begin (); iter != m_sendSocketList.end (); iter++ )
    {
        uint32_t interface = iter->second;

        if (m_interfaceExclusions.find (interface) == m_interfaceExclusions.end ())
        {
            uint16_t mtu = m_ipv4->GetMtu (interface);
            uint16_t maxRte = (mtu - Ipv4Header ().GetSerializedSize () - UdpHeader ().GetSerializedSize () - RipHeader ().GetSerializedSize ()) / RipRte ().GetSerializedSize ();

            Ptr<Packet> p = Create<Packet> ();
            SocketIpTtlTag tag;
            tag.SetTtl (1);
            p->AddPacketTag (tag);

            RipHeader hdr;
            hdr.SetCommand (RipHeader::RESPONSE);

            // Loop through all RTEs in the current router
            for (RoutesI rtIter = m_routes.begin (); rtIter != m_routes.end (); rtIter++)
            {
                if (rtIter->first->GetRouteStatus () == RipRoutingTableEntry::RIP_INVALID)
                {
                    //printf("Invalid route sending\n");
                    //continue;
                }
                bool splitHorizoning = (rtIter->first->GetInterface () == interface);
                Ipv4InterfaceAddress rtDestAddr = Ipv4InterfaceAddress(rtIter->first->GetDestNetwork (), rtIter->first->GetDestNetworkMask ());

                NS_LOG_DEBUG ("Processing RT " << rtDestAddr << " " << int(rtIter->first->IsRouteChanged ()));

                bool isGlobal = (rtDestAddr.GetScope () == Ipv4InterfaceAddress::GLOBAL);
                bool isDefaultRoute = ((rtIter->first->GetDestNetwork () == Ipv4Address::GetAny ()) &&
                                                (rtIter->first->GetDestNetworkMask () == Ipv4Mask::GetZero ()) &&
                                                (rtIter->first->GetInterface () != interface));

                bool sameNetwork = false;
                for (uint32_t index = 0; index < m_ipv4->GetNAddresses (interface); index++)
                {
                    Ipv4InterfaceAddress addr = m_ipv4->GetAddress (interface, index);
                    if (addr.GetLocal ().CombineMask (addr.GetMask ()) == rtIter->first->GetDestNetwork ())
                    {
                        sameNetwork = true;
                    }
                }

                if ((isGlobal || isDefaultRoute) &&
                    (periodic || rtIter->first->IsRouteChanged ()) &&
                    !sameNetwork)
                {
                    RipRte rte;
                    rte.SetPrefix (rtIter->first->GetDestNetwork ());
                    rte.SetSubnetMask (rtIter->first->GetDestNetworkMask ());
                    if (m_splitHorizonStrategy == POISON_REVERSE && splitHorizoning)
                    {
                        rte.SetRouteMetric (m_linkDown);
                    }
                    else
                    {
                        rte.SetRouteMetric (rtIter->first->GetRouteMetric ());
                    }
                    rte.SetRouteTag (rtIter->first->GetRouteTag ());

                    // Send the reference distance
                    if (rtIter->first->GetReferenceDistance() == 0)
                    {
                        rte.SetReferenceDistance (1);
                    }
                    else
                    {
                        rte.SetReferenceDistance (rtIter->first->GetRouteMetric ()); 
                    }

                    if (m_splitHorizonStrategy == SPLIT_HORIZON && !splitHorizoning)
                    {
                        hdr.AddRte (rte);
                    }
                    else if (m_splitHorizonStrategy != SPLIT_HORIZON)
                    {
                        hdr.AddRte (rte);
                    }
                }
                if (hdr.GetRteNumber () == maxRte)
                {
                    p->AddHeader (hdr);
                    NS_LOG_DEBUG ("SendTo: " << *p);
                    UpdateStat(SEND_RESP, hdr.GetSerializedSize());
                    iter->first->SendTo (p, 0, InetSocketAddress (RIP_ALL_NODE, RIP_PORT));
                    p->RemoveHeader (hdr);
                    hdr.ClearRtes ();
                }
            }
            if (hdr.GetRteNumber () > 0)
            {
                p->AddHeader (hdr);
                NS_LOG_DEBUG ("SendTo: " << *p);
                UpdateStat(SEND_RESP, hdr.GetSerializedSize());
                iter->first->SendTo (p, 0, InetSocketAddress (RIP_ALL_NODE, RIP_PORT));
            }
        }
    }
    for (RoutesI rtIter = m_routes.begin (); rtIter != m_routes.end (); rtIter++)
    {
        rtIter->first->SetRouteChanged (false);
    }
}

void Rip::DoSendHelloMessage (void)
{
    NS_LOG_FUNCTION (this << ("Hello"));

    for (SocketListI iter = m_sendSocketList.begin (); iter != m_sendSocketList.end (); iter++ )
    {
        uint32_t interface = iter->second;

        if (m_interfaceExclusions.find (interface) == m_interfaceExclusions.end ())
        {
            Ptr<Packet> p = Create<Packet> ();
            SocketIpTtlTag tag;
            tag.SetTtl (1);
            p->AddPacketTag (tag);

            RipHeader hdr;
            hdr.SetCommand (RipHeader::RESPONSE);

            RipRte rte;
            rte.SetPrefix (Ipv4Address::GetAny ());
            rte.SetSubnetMask (Ipv4Mask::GetZero ());
            rte.SetRouteMetric (m_linkDown);
            rte.SetReferenceDistance (m_linkDown);

            hdr.AddRte (rte); 
            p->AddHeader (hdr);
            NS_LOG_DEBUG ("SendTo: " << *p);
            UpdateStat(SEND_RESP, hdr.GetSerializedSize());
            iter->first->SendTo (p, 0, InetSocketAddress (RIP_ALL_NODE, RIP_PORT));
        }
    }
}

void Rip::SendTriggeredRouteUpdate ()
{
    NS_LOG_FUNCTION (this);

    if (m_nextTriggeredUpdate.IsRunning())
    {
        NS_LOG_LOGIC ("Skipping Triggered Update due to cooldown");
        return;
    }

    // DoSendRouteUpdate (false);

    // note: The RFC states:
    //     After a triggered
    //     update is sent, a timer should be set for a random interval between 1
    //     and 5 seconds.  If other changes that would trigger updates occur
    //     before the timer expires, a single update is triggered when the timer
    //     expires.  The timer is then reset to another random value between 1
    //     and 5 seconds.  Triggered updates may be suppressed if a regular
    //     update is due by the time the triggered update would be sent.
    // Here we rely on this:
    // When an update occurs (either Triggered or Periodic) the "IsChanged ()"
    // route field will be cleared.
    // Hence, the following Triggered Update will be fired, but will not send
    // any route update.

    Time delay = Seconds (m_rng->GetValue (m_minTriggeredUpdateDelay.GetSeconds (), m_maxTriggeredUpdateDelay.GetSeconds ()));
    m_nextTriggeredUpdate = Simulator::Schedule (delay, &Rip::DoSendRouteUpdate, this, false);
}

void Rip::SendUnsolicitedRouteUpdate ()
{
    NS_LOG_FUNCTION (this);

    if (m_nextTriggeredUpdate.IsRunning())
    {
        m_nextTriggeredUpdate.Cancel ();
    }

    //DoSendRouteUpdate (true);
    // Use hello messages rather than periodic updates
    DoSendHelloMessage ();

    Time delay = m_unsolicitedUpdate + Seconds (m_rng->GetValue (0, 0.5*m_unsolicitedUpdate.GetSeconds ()) );
    m_nextUnsolicitedUpdate = Simulator::Schedule (delay, &Rip::SendUnsolicitedRouteUpdate, this);
}

std::set<uint32_t> Rip::GetInterfaceExclusions () const
{
    return m_interfaceExclusions;
}

void Rip::SetInterfaceExclusions (std::set<uint32_t> exceptions)
{
    NS_LOG_FUNCTION (this);

    m_interfaceExclusions = exceptions;
}

uint8_t Rip::GetInterfaceMetric (uint32_t interface) const
{
    NS_LOG_FUNCTION (this << interface);

    std::map<uint32_t, uint8_t>::const_iterator iter = m_interfaceMetrics.find (interface);
    if (iter != m_interfaceMetrics.end ())
    {
        return iter->second;
    }
    return 1;
}

void Rip::SetInterfaceMetric (uint32_t interface, uint8_t metric)
{
    NS_LOG_FUNCTION (this << interface << int (metric));

    if (metric < m_linkDown)
    {
        m_interfaceMetrics[interface] = metric;
    }
}

void Rip::SendFullTableRequest () // SendRouteRequest
{
    NS_LOG_FUNCTION (this);

    Ptr<Packet> p = Create<Packet> ();
    SocketIpTtlTag tag;
    p->RemovePacketTag (tag);
    tag.SetTtl (1);
    p->AddPacketTag (tag);

    RipHeader hdr;
    hdr.SetCommand (RipHeader::REQUEST);

    RipRte rte;
    rte.SetPrefix (Ipv4Address::GetAny ());
    rte.SetSubnetMask (Ipv4Mask::GetZero ());
    rte.SetRouteMetric (m_linkDown);

    // Set reference distance to 16 because we do not care about reference distances 
    // when requesting a full routing table.
    rte.SetReferenceDistance (m_linkDown); 

    hdr.AddRte (rte);
    p->AddHeader (hdr);

    for (SocketListI iter = m_sendSocketList.begin (); iter != m_sendSocketList.end (); iter++ )
    {
        uint32_t interface = iter->second;

        if (m_interfaceExclusions.find (interface) == m_interfaceExclusions.end ())
        {
            NS_LOG_DEBUG ("SendTo: " << *p);
            UpdateStat(SEND_REQ, hdr.GetSerializedSize());
            iter->first->SendTo (p, 0, InetSocketAddress (RIP_ALL_NODE, RIP_PORT));
        }
    }
}

// Add a function that will be able to request an indiviudal table entry.
// Inputs: Ipv4Address destination, Ipv4Mask destMask, uint32_t desiredRefDist in RTE form
void Rip::SendIndividualRouteRequest (RipRte rtes, uint32_t iFace)
{
    NS_LOG_FUNCTION (this);

    bool in_prt = false;
    // Check if we already have a pending request for this destination. If we do then do not send another request.
   for (PendingRequestsI it = m_pendingRequestTable.begin (); it != m_pendingRequestTable.end (); it++)
    {
        // Check if the incoming RTE is in the pending request table.
        if (it->first->GetNetwork () == rtes.GetPrefix () && it->first->GetNetworkMask () == rtes.GetSubnetMask ())
        {
            // Check if the reference distance is able to satisfy the initial request.
            if (rtes.GetReferenceDistance () >= it->first->GetReferenceDistance ())
            {
                if (it->first->GetForwardTimer () == true)
                {
                    return;
                }
                else
                {
                    in_prt = true;
                }
            }
        }	
    }

    Ptr<Packet> p = Create<Packet> ();
    SocketIpTtlTag tag;
    p->RemovePacketTag (tag);
    tag.SetTtl (m_linkDown - 1); // Send the request up to the maximum RIP distance
    p->AddPacketTag (tag);

    //std::cout << Now ().As(Time::S) << " Sending request in node " << m_ipv4->GetObject<Node> ()->GetId () << " for destination " << rtes.GetPrefix () << "\n";


    RipHeader hdr;
    hdr.SetCommand (RipHeader::REQUEST);

    hdr.AddRte (rtes);
    p->AddHeader (hdr);

    // Broadcast the request out of all interfaces.
    for (SocketListI iter = m_sendSocketList.begin (); iter != m_sendSocketList.end (); iter++ )
    {
        uint32_t interface = iter->second;

        if (m_interfaceExclusions.find (interface) == m_interfaceExclusions.end ())
        {
            NS_LOG_DEBUG ("SendTo: " << *p);
            UpdateStat(SEND_REQ, hdr.GetSerializedSize());
            iter->first->SendTo (p, 0, InetSocketAddress (RIP_ALL_NODE, RIP_PORT));
        }
    }
    
    if (in_prt == false)
    {
        RipPendingRequestEntry* new_prt_entry = new RipPendingRequestEntry ();
        new_prt_entry->SetNetwork (rtes.GetPrefix ());
        new_prt_entry->SetNetworkMask (rtes.GetSubnetMask ());
        new_prt_entry->SetReferenceDistance (rtes.GetReferenceDistance ());
        new_prt_entry->SetRouteInterface (0);
        new_prt_entry->SetForwardTimer (true);

        m_pendingRequestTable.push_front (std::make_pair (new_prt_entry, EventId ()));
        EventId allowRequest = Simulator::Schedule (m_forwardingDelay, &Rip::AllowPRTSend, this, new_prt_entry);
        (m_pendingRequestTable.begin ())->second = allowRequest;
    }
}

void Rip::AddDefaultRouteTo (Ipv4Address nextHop, uint32_t interface)
{
    NS_LOG_FUNCTION (this << interface);

    AddNetworkRouteTo (Ipv4Address ("0.0.0.0"), Ipv4Mask::GetZero (), nextHop, interface);
}

/* Code used to track the number of signaling overhead packets passed around and the size of these packets */

uint32_t Rip::GetNumRequestsSent ()
{
    return m_requestSentNumber;
}

uint32_t Rip::GetRequestsSentSize ()
{
    return m_requestSentSize;
}

uint32_t Rip::GetNumRequestsRecv ()
{
    return m_requestRecvNumber;
}

uint32_t Rip::GetRequestsRecvSize ()
{
    return m_requestRecvSize;
}

uint32_t Rip::GetNumResponsesSent ()
{
    return m_responseSentNumber;
}

uint32_t Rip::GetNumResponsesRecv ()
{
    return m_responseRecvNumber;
}

uint32_t Rip::GetResponsesRecvSize ()
{
    return m_responseRecvSize;
}

uint32_t Rip::GetResponsesSentSize ()
{
    return m_responseSentSize;
}



void Rip::UpdateStat(Stat_Event_ID event, uint32_t msg_size)
{
    if (Now () < Seconds(0.0))
    {
        return;
    }

    // Log the proper type of signaling overhead.
    switch (event){
        case SEND_REQ:
            UpdateStatOnSendRequest (msg_size);
            break;
        case RECV_REQ:
            UpdateStatOnReceiveRequest (msg_size);
            break;
        case SEND_RESP:
            UpdateStatOnSendResponse (msg_size);
            break;
        case RECV_RESP:
            UpdateStatOnReceiveResponse (msg_size);
            break;
        default:
            NS_LOG_ERROR ("Unknown EventId");
    }
    return;
}


void Rip::UpdateStatOnSendRequest (uint32_t msg_size)
{
    m_requestSentNumber += 1;
    m_requestSentSize += msg_size;
    return;
}

void Rip::UpdateStatOnReceiveRequest (uint32_t msg_size)
{
    m_requestRecvNumber += 1;
    m_requestRecvSize += msg_size;
    return;
}

void Rip::UpdateStatOnSendResponse (uint32_t msg_size)
{
    m_responseSentNumber += 1;
    m_responseSentSize += msg_size;
    return;
}

void Rip::UpdateStatOnReceiveResponse (uint32_t msg_size)
{
    m_responseRecvNumber += 1;
    m_responseRecvSize += msg_size;
    return;
}

void Rip::SetForwardRequestTracker(bool val)
{
    if (val != GetForwardRequestTracker ())
    {
        forwardRequest = val;
    }
}

bool Rip::GetForwardRequestTracker(void)
{
    return forwardRequest;
}


#if 1
/*
* RipRoutingTableEntry
*/

RipRoutingTableEntry::RipRoutingTableEntry ()
: m_tag (0), m_metric (0), m_referenceDistance (16), m_status (RIP_INVALID), m_changed (false)
{
}

RipRoutingTableEntry::RipRoutingTableEntry (Ipv4Address network, Ipv4Mask networkPrefix, Ipv4Address nextHop, uint32_t interface)
: Ipv4RoutingTableEntry ( Ipv4RoutingTableEntry::CreateNetworkRouteTo (network, networkPrefix, nextHop, interface) ),
m_tag (0), m_metric (0), m_referenceDistance (16), m_status (RIP_INVALID), m_changed (false)
{
}

RipRoutingTableEntry::RipRoutingTableEntry (Ipv4Address network, Ipv4Mask networkPrefix, uint32_t interface)
: Ipv4RoutingTableEntry ( Ipv4RoutingTableEntry::CreateNetworkRouteTo (network, networkPrefix, interface) ),
m_tag (0), m_metric (0), m_referenceDistance (16), m_status (RIP_INVALID), m_changed (false)
{
}

RipRoutingTableEntry::~RipRoutingTableEntry ()
{
}


void RipRoutingTableEntry::SetRouteTag (uint16_t routeTag)
{
    if (m_tag != routeTag)
    {
        m_tag = routeTag;
        m_changed = true;
    }
}

uint16_t RipRoutingTableEntry::GetRouteTag () const
{
    return m_tag;
}

void RipRoutingTableEntry::SetRouteMetric (uint8_t routeMetric)
{
    if (m_metric != routeMetric)
    {
        m_metric = routeMetric;
        m_changed = true;
    }
}

uint8_t RipRoutingTableEntry::GetRouteMetric () const
{
    return m_metric;
}

void RipRoutingTableEntry::SetReferenceDistance (uint8_t refDist)
{
    if (m_referenceDistance != refDist)
    {
        m_referenceDistance = refDist;
        m_changed = true;
    }
}

uint8_t RipRoutingTableEntry::GetReferenceDistance (void) const
{
    return m_referenceDistance;
}

void RipRoutingTableEntry::SetRouteStatus (Status_e status)
{
    if (m_status != status)
    {
        m_status = status;
        m_changed = true;
    }
}

RipRoutingTableEntry::Status_e RipRoutingTableEntry::GetRouteStatus (void) const
{
    return m_status;
}

void RipRoutingTableEntry::SetRouteChanged (bool changed)
{
    m_changed = changed;
}

bool RipRoutingTableEntry::IsRouteChanged (void) const
{
    return m_changed;
}


std::ostream & operator << (std::ostream& os, const RipRoutingTableEntry& rte)
{
    os << static_cast<const Ipv4RoutingTableEntry &>(rte);
    os << ", metric: " << int (rte.GetRouteMetric ()) << ", tag: " << int (rte.GetRouteTag ())
        << ", Reference distance: " << int (rte.GetReferenceDistance ());

    return os;
}


/*
* RipPendingRequestEntry
*/

RipPendingRequestEntry::RipPendingRequestEntry ()
{
}

RipPendingRequestEntry::RipPendingRequestEntry (Ipv4Address requestNetwork, Ipv4Mask requestNetworkMask, uint32_t interface, uint8_t requestingRefDist)
: p_requestNetwork ("127.0.0.1"), p_requestNetworkMask("0.0.0.0"), p_referenceDistance (16), p_interface (0)
{
}

RipPendingRequestEntry::~RipPendingRequestEntry ()
{
}

void RipPendingRequestEntry::SetRouteInterface (uint32_t interface)
{
    if (p_interface != interface)
    {
        p_interface = interface;
    }
}

uint32_t RipPendingRequestEntry::GetRouteInterface (void) const
{
    return p_interface;
}

void RipPendingRequestEntry::SetNetwork (Ipv4Address requestNetwork)
{
    if (p_requestNetwork != requestNetwork)
    {
        p_requestNetwork = requestNetwork;
    }
}

Ipv4Address RipPendingRequestEntry::GetNetwork () const
{
    return p_requestNetwork;
}

void RipPendingRequestEntry::SetNetworkMask (Ipv4Mask netMask)
{
    if (p_requestNetworkMask != netMask)
    {
        p_requestNetworkMask = netMask;
    }
}

Ipv4Mask RipPendingRequestEntry::GetNetworkMask (void) const
{
    return p_requestNetworkMask;
}

void RipPendingRequestEntry::SetReferenceDistance (uint8_t refDist)
{
    if (p_referenceDistance != refDist)
    {
        p_referenceDistance = refDist;
    }
}

uint8_t RipPendingRequestEntry::GetReferenceDistance (void) const
{
    return p_referenceDistance;
}

void RipPendingRequestEntry::SetForwardTimer(bool event)
{
    if (p_recentlyForwarded != event)
    {
        p_recentlyForwarded = event;
    }
}

bool RipPendingRequestEntry::GetForwardTimer(void)
{
    return p_recentlyForwarded;
}

std::ostream & operator << (std::ostream& os, const RipPendingRequestEntry& pre)
{
    os << "Network: " << pre.GetNetwork() << "/" << pre.GetNetworkMask ();
    os << ", interface: " << int (pre.GetRouteInterface ())
        << ", Reference distance: " << int (pre.GetReferenceDistance ());

    return os;
}

#endif

/*
* RipRipDistanceTableEntry
*/

RipDistanceTableEntry::RipDistanceTableEntry () :
d_requestNetwork (Ipv4Address("0.0.0.0")), d_requestNetworkMask(), d_gateway (Ipv4Address("0.0.0.0")), d_distance (16), d_referenceDistance (16), d_interface (0), d_valid(false)
{
}

RipDistanceTableEntry::RipDistanceTableEntry (Ipv4Address requestNetwork, Ipv4Mask requestNetworkMask, Ipv4Address gateway, uint32_t interface, uint32_t distance, uint8_t requestingRefDist)
: d_requestNetwork (requestNetwork), d_requestNetworkMask(requestNetworkMask), d_gateway (gateway), d_distance (distance), d_referenceDistance (requestingRefDist), d_interface (interface), d_valid(false)
{
}

RipDistanceTableEntry::~RipDistanceTableEntry ()
{
}

void RipDistanceTableEntry::Print (std::ostream& os)
{
    os << "Network: " << d_requestNetwork << "/" << d_requestNetworkMask << ", Gateway: " << d_gateway;
    os << ", interface: " << int (d_interface) << ", Distance: " << int (d_distance)
        << ", Reference distance: " << int (d_referenceDistance) << ", Valid: " << d_valid;
}

void RipDistanceTableEntry::SetRouteInterface (uint32_t interface)
{
    if (d_interface != interface)
    {
        d_interface = interface;
    }
}

uint32_t RipDistanceTableEntry::GetRouteInterface (void) const
{
    return d_interface;
}

void RipDistanceTableEntry::SetNetwork (Ipv4Address requestNetwork)
{
    if (d_requestNetwork != requestNetwork)
    {
        d_requestNetwork = requestNetwork;
    }
}

Ipv4Address RipDistanceTableEntry::GetNetwork () const
{
    return d_requestNetwork;
}

void RipDistanceTableEntry::SetNetworkMask (Ipv4Mask netMask)
{
    if (d_requestNetworkMask != netMask)
    {
        d_requestNetworkMask = netMask;
    }
}

Ipv4Mask RipDistanceTableEntry::GetNetworkMask (void) const
{
    return d_requestNetworkMask;
}

void RipDistanceTableEntry::SetGateway (Ipv4Address gateway)
{
    if (d_gateway != gateway)
    {
        d_gateway = gateway;
    }
}

Ipv4Address RipDistanceTableEntry::GetGateway () const
{
    return d_gateway;
}

void RipDistanceTableEntry::SetRouteDistance (uint32_t dist)
{
    if (d_distance != dist)
    {
        d_distance = dist;
    }
}

uint32_t RipDistanceTableEntry::GetRouteDistance (void) const
{
    return d_distance;
}

void RipDistanceTableEntry::SetReferenceDistance (uint8_t refDist)
{
    if (d_referenceDistance != refDist)
    {
        d_referenceDistance = refDist;
    }
}

uint8_t RipDistanceTableEntry::GetReferenceDistance (void) const
{
    return d_referenceDistance;
}

void RipDistanceTableEntry::SetValid (bool valid)
{
    if (d_valid != valid)
    {
        d_valid = valid;
    }
}

bool RipDistanceTableEntry::GetValid (void) const
{
    return d_valid;
}

std::ostream & operator << (std::ostream& os, const RipDistanceTableEntry& pre)
{
    os << "Network: " << pre.GetNetwork() << "/" << pre.GetNetworkMask () << ", Gateway: " << pre.GetGateway ();
    os << ", interface: " << int (pre.GetRouteInterface ()) << ", Distance: " << int (pre.GetRouteDistance ())
        << ", Reference distance: " << int (pre.GetReferenceDistance ()) << ", Valid: " << pre.GetValid ();

    return os;
}


/*
* DistanceTable
*/
DistanceTable::DistanceTable ()
{
}

DistanceTable::~DistanceTable()
{
}

void DistanceTable::Print(Ptr<OutputStreamWrapper> stream, Time::Unit unit, uint32_t nodeID)
{
    std::ostream* os = stream->GetStream ();

    *os << "Node: " << nodeID
        << ", Time: " << Now().As (unit)
        << ", RIP Distance table" << std::endl;

    if (!DistanceTableEntries.empty ())
    {
        *os << "Destination     Mask            Gateway         Valid Metric RefDist Iface" << std::endl;
        for (DistanceTableEntriesI it = DistanceTableEntries.begin (); it != DistanceTableEntries.end (); it++)
        {
            RipDistanceTableEntry *route = *it;

            if (true)
            {
                std::ostringstream dest, gw, mask, flags;
                dest << route->GetNetwork ();
                *os << std::setiosflags (std::ios::left) << std::setw (16) << dest.str ();
                mask << route->GetNetworkMask ();
                *os << std::setiosflags (std::ios::left) << std::setw (16) << mask.str ();
                gw << route->GetGateway ();
                *os << std::setiosflags (std::ios::left) << std::setw (16) << gw.str ();
                flags << route->GetValid ();
                *os << std::setiosflags (std::ios::left) << std::setw (6) << flags.str ();

                *os << std::setiosflags (std::ios::left) << std::setw (7) << int(route->GetRouteDistance ());
                *os << std::setiosflags (std::ios::left) << std::setw (8) << int(route->GetReferenceDistance());
                *os << std::setiosflags (std::ios::left) << std::setw (6) << route->GetRouteInterface ();

                *os << std::endl;
            }
        }
    }
    *os << std::endl;
}

// Add a new entry to the distance table
void DistanceTable::AddDistanceTableEntry(RipDistanceTableEntry *entry, uint32_t nodeID)
{
    if (debug) std::cout << "Adding distance table entry " << *entry << "\n";

    Ipv4Address newAddr = entry->GetNetwork ().CombineMask(entry->GetNetworkMask ());
   
    // Track if we found and changed the distance table entry
    bool found = false;
    bool changed = false;

    // Loop through the distance table
    for (DistanceTableEntriesI it = DistanceTableEntries.begin (); it != DistanceTableEntries.end (); it++)
    {
        // First check if the proper address is used

        Ipv4Address dtAddr = (*it)->GetNetwork ().CombineMask ((*it)->GetNetworkMask ());
        if (newAddr == dtAddr)
        {
            // Then check if the interface (same neighbor) matches
            if (entry->GetRouteInterface () == (*it)->GetRouteInterface ())
            {
                // Mark that we found the route already exists, try to check if we can update the route
                found = true;

                // First check if the destination is no longer reachable, if not reachable then just delete the entry
                if (entry->GetRouteDistance () >= destUnreachable && ((*it)->GetRouteDistance () < destUnreachable))
                {
                    changed = true;
                    if (debug && nodeID == 2) std::cout << "Deleting entry: " << **it << "\n";
                    DeleteDistanceTableEntry(*it);
                    break;
                }
                // First check the RD
                uint8_t newRD = entry->GetReferenceDistance ();
                uint8_t oldRD = (*it)->GetReferenceDistance ();
                if (newRD == oldRD)
                {
                    // Only mark changed if the RD is the same but the D was different
                    uint32_t newD = entry->GetRouteDistance ();
                    if (newD != (*it)->GetRouteDistance ())
                    {
                        changed = true;
                        (*it)->SetRouteDistance(newD);
                        if (debug && nodeID == 2)  std::cout << "Changed route distance to " << (*it)->GetRouteDistance () << "\n";
                    }
                }
                else if (newRD < oldRD)
                {
                    // If the RD was lower, then update the RD and mark changed
                    changed = true;
                    (*it)->SetReferenceDistance(newRD);
                    if (debug && nodeID == 2)  std::cout <<  "Changed ref distance to " << (*it)->GetReferenceDistance () << "\n";

                    // Still check if we can update the D
                    uint32_t newD = entry->GetRouteDistance ();
                    if (newD != (*it)->GetRouteDistance ())
                    {
                        (*it)->SetRouteDistance(newD);
                        if (debug && nodeID == 2) std::cout << "Changed route distance 2 " << (*it)->GetRouteDistance () << "\n";
                    }
                }
                break; // If we found the proper entry, then we can finish searching
            }
        }
    }
    // If we couldnt find the entry, then add it into the distance table
    if (!found)
    {
        RipDistanceTableEntry *p = entry;//new RipDistanceTableEntry (entry->GetNetwork (), entry->GetNetworkMask (), entry->GetGateway (), entry->GetRouteInterface (), entry->GetRouteDistance (), entry->GetReferenceDistance ());
        if (debug && nodeID == 2) std::cout << "Adding new entry " << *p << "\n";
        DistanceTableEntries.push_front(p);
    }

    // If we found and updated the entry, then we need to run a check to find if the entry should become valid or invalid
    if (changed || !found)
    {
        ValidateDistanceTable();
    }

    if (!changed && found)
    {
        if (debug && nodeID == 2) std::cout << "Already had route for address" << newAddr << " with iface " << entry->GetRouteInterface ()<< " with gw " << entry->GetGateway () << "\n";
    }
}

// Delete an entry in the distance table
void DistanceTable::DeleteDistanceTableEntry(RipDistanceTableEntry *entry)
{
    NS_LOG_FUNCTION (*entry);

    if (debug) std::cout << "Deleting distance table entry " << *entry << "\n";

    Ipv4Address newAddr = entry->GetNetwork ().CombineMask(entry->GetNetworkMask ());

    for (DistanceTableEntriesI it = DistanceTableEntries.begin (); it != DistanceTableEntries.end (); it++)
    {
        // First check if the proper address is used
        Ipv4Address dtAddr = (*it)->GetNetwork ();

        if (newAddr == dtAddr)
        {
            // Then check if the interface (same neighbor) matches
            if (entry->GetRouteInterface () == (*it)->GetRouteInterface ())
            {
                DistanceTableEntries.erase (it);
                return;
            }
        }
    }
    if (debug) std::cout << "DeleteDTEntry " << newAddr << " Couldnt find entry to delete from DT!\n";
}

// Return the interfaces connected to the that satisfy this route
RipRoutingTableEntry* DistanceTable::GetDistanceTableRoute(Ipv4Address destination)
{
    // Want a list of Ipv4 routes aka RipRoutingTableEntry, so get the list from the distance table, then return a single entry
    // Use a random function to pick which route to return and therefore use to route the packet

    std::list<RipDistanceTableEntry*> validRoutes;
    for (DistanceTableEntriesI it = DistanceTableEntries.begin (); it != DistanceTableEntries.end (); it++)
    {
        Ipv4Address dtAddr = (*it)->GetNetwork ().CombineMask((*it)->GetNetworkMask ());

        if ((*it)->GetValid () == true)
        {
            Ipv4Mask mask = (*it)->GetNetworkMask ();
            if (mask.IsMatch (destination, dtAddr))
            {
                validRoutes.push_front(*it);
            }				
        }
    }

    Ptr<UniformRandomVariable> rng = CreateObject<UniformRandomVariable> ();

    RipRoutingTableEntry *entry = NULL;
    uint8_t numValidEntries = validRoutes.size ();
    if (numValidEntries == 0)
    {	
        return entry;
    }
    else if (numValidEntries == 1)
    {		
        Ipv4Address network = validRoutes.front()->GetNetwork ();
        Ipv4Mask networkPrefix = validRoutes.front()->GetNetworkMask ();
        Ipv4Address gateway = validRoutes.front()->GetGateway ();
        uint32_t interface = validRoutes.front()->GetRouteInterface ();
        entry = new RipRoutingTableEntry (network, networkPrefix, gateway, interface);
        entry->SetReferenceDistance (validRoutes.front()->GetReferenceDistance ()); // Set the reference distance
        entry->SetRouteMetric (validRoutes.front()->GetRouteDistance ());
        entry->SetRouteStatus (RipRoutingTableEntry::RIP_VALID);
    }
    else if (numValidEntries == 2)
    {
        double pick = rng->GetValue (1.0, 2.0);
        if (pick < 1.5)
        {
            Ipv4Address network = validRoutes.front()->GetNetwork ();
            Ipv4Mask networkPrefix = validRoutes.front()->GetNetworkMask ();
            Ipv4Address gateway = validRoutes.front()->GetGateway ();
            uint32_t interface = validRoutes.front()->GetRouteInterface ();
            entry = new RipRoutingTableEntry (network, networkPrefix, gateway, interface);
            entry->SetReferenceDistance (validRoutes.front()->GetReferenceDistance ()); // Set the reference distance
            entry->SetRouteMetric (validRoutes.front()->GetRouteDistance ());
            entry->SetRouteStatus (RipRoutingTableEntry::RIP_VALID);
        }
        else
        {
            Ipv4Address network = validRoutes.back()->GetNetwork ();
            Ipv4Mask networkPrefix = validRoutes.back()->GetNetworkMask ();
            Ipv4Address gateway = validRoutes.back()->GetGateway ();
            uint32_t interface = validRoutes.back()->GetRouteInterface ();
            entry = new RipRoutingTableEntry (network, networkPrefix, gateway, interface);
            entry->SetReferenceDistance (validRoutes.back()->GetReferenceDistance ()); // Set the reference distance
            entry->SetRouteMetric (validRoutes.back()->GetRouteDistance ());
            entry->SetRouteStatus (RipRoutingTableEntry::RIP_VALID);
        }
    }
    else if (numValidEntries == 3)
    {
        double pick = rng->GetValue (1.0, 2.0);
        if (pick < 1.33)
        {
            Ipv4Address network = validRoutes.front()->GetNetwork ();
            Ipv4Mask networkPrefix = validRoutes.front()->GetNetworkMask ();
            Ipv4Address gateway = validRoutes.front()->GetGateway ();
            uint32_t interface = validRoutes.front()->GetRouteInterface ();
            entry = new RipRoutingTableEntry (network, networkPrefix, gateway, interface);
            entry->SetReferenceDistance (validRoutes.front()->GetReferenceDistance ()); // Set the reference distance
            entry->SetRouteMetric (validRoutes.front()->GetRouteDistance ());
            entry->SetRouteStatus (RipRoutingTableEntry::RIP_VALID);
            entry->SetRouteChanged (true);
        }
        else if (pick >= 1.33 && pick < 1.67)
        {
            validRoutes.pop_front();
            Ipv4Address network = validRoutes.front()->GetNetwork ();
            Ipv4Mask networkPrefix = validRoutes.front()->GetNetworkMask ();
            Ipv4Address gateway = validRoutes.front()->GetGateway ();
            uint32_t interface = validRoutes.front()->GetRouteInterface ();
            entry = new RipRoutingTableEntry (network, networkPrefix, gateway, interface);
            entry->SetReferenceDistance (validRoutes.front()->GetReferenceDistance ()); // Set the reference distance
            entry->SetRouteMetric (validRoutes.front()->GetRouteDistance ());
            entry->SetRouteStatus (RipRoutingTableEntry::RIP_VALID);
            entry->SetRouteChanged (true);
        }
        else
        {
            Ipv4Address network = validRoutes.back()->GetNetwork ();
            Ipv4Mask networkPrefix = validRoutes.back()->GetNetworkMask ();
            Ipv4Address gateway = validRoutes.back()->GetGateway ();
            uint32_t interface = validRoutes.back()->GetRouteInterface ();
            entry = new RipRoutingTableEntry (network, networkPrefix, gateway, interface);
            entry->SetReferenceDistance (validRoutes.back()->GetReferenceDistance ()); // Set the reference distance
            entry->SetRouteMetric (validRoutes.back()->GetRouteDistance ());
            entry->SetRouteStatus (RipRoutingTableEntry::RIP_VALID);
            entry->SetRouteChanged (true);
        }
    }
    if (debug) std::cout << "Returning valid entry: " << *entry << "\n";
    return entry;
}

uint32_t DistanceTable::GetNumValidDTEntries(Ipv4Address destination)
{
    uint32_t count = 0;
    for (DistanceTableEntriesI it = DistanceTableEntries.begin (); it != DistanceTableEntries.end (); it++)
    {
        Ipv4Address dtAddr = (*it)->GetNetwork ().CombineMask((*it)->GetNetworkMask ());

        if ((*it)->GetValid () == true)
        {
            Ipv4Mask mask = (*it)->GetNetworkMask ();
            if (mask.IsMatch (destination, dtAddr))
            {
            count++;
            }				
        }
    }	
    return count;
}

// Invalidate an entry in the distance table
void DistanceTable::InvalidateDTEntry (RipDistanceTableEntry *entry)
{
    NS_LOG_FUNCTION (*entry);

    if (debug) std::cout << "Invalidating distance table entry " << *entry << "\n";

    for (DistanceTableEntriesI it = DistanceTableEntries.begin (); it != DistanceTableEntries.end (); it++)
    {
        if ((*it) == entry)
        {
            (*it)->SetValid(false);
            return;
        }
    }
    std::cout << "InvalidateDTEntry <> Couldnt find entry to invalidate from DT!\n";
}

// Validate entire distance table
void DistanceTable::ValidateDistanceTable(void)
{
    if (debug) std::cout << "Validating distance table\n";

    // First invalidate all entries
    for (DistanceTableEntriesI it = DistanceTableEntries.begin (); it != DistanceTableEntries.end (); it++)
    {
        (*it)->SetValid(false);
    }

    // Now traverse the list and mark which entries will be valid
    std::list<Ipv4Address> checkedAddresses;

    for (DistanceTableEntriesI it = DistanceTableEntries.begin (); it != DistanceTableEntries.end (); it++)
    {
        // First check if we have already tried looking for this address. If we have, just move on to the next one.
        Ipv4Address dtAddr = (*it)->GetNetwork ().CombineMask ((*it)->GetNetworkMask ());
        if (! checkedAddresses.empty ())
        {
            bool found = false;
            auto checkIter = checkedAddresses.begin();
            for (; checkIter != checkedAddresses.end(); checkIter++)
            {
                if ((*checkIter) == dtAddr)
                {
                    found = true;
                    break;
                }
            }
                if (found)
                {
                    continue;
                }
        }

        // Start by adding the address to our list of checked addresses
        checkedAddresses.push_front(dtAddr);

        // Create a new list to track all valid entries
        std::list<RipDistanceTableEntry *> validEntries;
        validEntries.push_front(*it);

        uint32_t currentHighestDistance = (*it)->GetRouteDistance ();
        uint8_t currentRD = (*it)->GetReferenceDistance ();
        uint8_t validEntriesCount = 1;

        // Create a new iterator and go through the rest of the list
        DistanceTableEntriesI secondIter = it;
        secondIter++;
        for (; secondIter != DistanceTableEntries.end (); secondIter++)
        {
            // First find if the proper address is found
            Ipv4Address checkAddr = (*secondIter)->GetNetwork ().CombineMask ((*secondIter)->GetNetworkMask ());
            if (checkAddr == dtAddr)
            {
                // Now check if the RD is the same, if it is then we can potentially use the route
                uint8_t checkRD = (*secondIter)->GetReferenceDistance ();
                if (checkRD == currentRD)
                {
                    // Check if the max number of entries are already in use
                    if (validEntriesCount >= multiPathNumber)
                    {
                        // Now check the route distance to see if it beats the current routes
                        if ((*secondIter)->GetRouteDistance () < currentHighestDistance)
                        {
                            // If we beat the highest distance, then get rid of previous worst path, and add this new one
                            auto checkIter = validEntries.begin();
                            for (; checkIter != validEntries.end(); checkIter++)
                            {
                                if ((*checkIter)->GetRouteDistance () == currentHighestDistance)
                                {
                                    break;
                                }
                            }
                            if (checkIter != DistanceTableEntries.end ())
                            {
                                validEntries.erase(checkIter); // Delete previous highest
                                validEntries.push_front(*secondIter); // Add new entry
                                currentHighestDistance = (*secondIter)->GetRouteDistance (); // Reset the new highest distance to current
                                // Traverse the list of entries and set the new highest distance
                                for (auto i = validEntries.begin (); i != validEntries.end (); i++)
                                {
                                    if ((*i)->GetRouteDistance () > currentHighestDistance)
                                    {
                                        currentHighestDistance = (*i)->GetRouteDistance ();
                                    }
                                }
                            }
                        }
                    }
                    else // Not at the max number of multiple paths, simply add it
                    {
                        validEntries.push_front(*secondIter);
                        validEntriesCount++;
                        if ((*secondIter)->GetRouteDistance () > currentHighestDistance)
                        {
                            currentHighestDistance = (*secondIter)->GetRouteDistance ();
                        }
                    }

                }
                // If the RD is lower than we need to clear the current entries and start the search process 
                // over, do not use multi-path routing for diff RDs.
                else if (checkRD < currentRD)
                {
                    currentRD = checkRD;
                    currentHighestDistance = (*secondIter)->GetRouteDistance ();
                    validEntriesCount = 1;
                    validEntries.clear();
                    validEntries.push_front(*secondIter);
                }
            }
        }
        for (auto finalIter = validEntries.begin (); finalIter != validEntries.end (); finalIter++)
        {
            if ((*finalIter)->GetRouteDistance () < destUnreachable)
                (*finalIter)->SetValid(true);
        }
    }
    return;
}

// Validate entries corresponding to a specific address
void DistanceTable::ValidateDistanceTable(Ipv4Address destination, Ipv4Mask destinationMask)
{
    Ipv4Address newAddr = destination.CombineMask (destinationMask);

    // First invalidate all entries
    for (DistanceTableEntriesI it = DistanceTableEntries.begin (); it != DistanceTableEntries.end (); it++)
    {
        // First check if we have already tried looking for this address. If we have, just move on to the next one.
        Ipv4Address dtAddr = (*it)->GetNetwork ().CombineMask ((*it)->GetNetworkMask ());
        if (newAddr == dtAddr)
        {
            (*it)->SetValid(false);
        }
    }

    for (DistanceTableEntriesI it = DistanceTableEntries.begin (); it != DistanceTableEntries.end (); it++)
    {
        // First check if we have already tried looking for this address. If we have, just move on to the next one.
        Ipv4Address dtAddr = (*it)->GetNetwork ().CombineMask ((*it)->GetNetworkMask ());
        if (newAddr == dtAddr)
        {
            printf("Matched address\n");
        }
    }

    return;
}


}

