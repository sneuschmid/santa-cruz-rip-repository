/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
* Copyright (c) 2016 Universita' di Firenze, Italy
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation;
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
* Author: Tommaso Pecorella <tommaso.pecorella@unifi.it>
*/

#ifndef RIP_H
#define RIP_H

#include <list>

#include "ns3/ipv4-routing-protocol.h"
#include "ns3/ipv4-interface.h"
#include "ns3/inet-socket-address.h"
#include "ns3/ipv4-l3-protocol.h"
#include "ns3/ipv4-routing-table-entry.h"
#include "ns3/random-variable-stream.h"
#include "ns3/scrip-header.h"

namespace ns3 {

/**
* \ingroup ipv4Routing
* \defgroup rip RIP
*
* The RIP protocol (\RFC{2453}) is a unicast-only IPv4 IGP (Interior Gateway Protocol).
* Its convergence time is rather long. As a consequence, it is suggested to
* carefully check the network topology and the route status before sending
* data flows.
*
* RIP implements the Bellman-Ford algorithm (although the RFC does not state it).
* Bellman-Ford algorithm convergence time is O(|V|*|E|) where |V| and |E| are the
* number of vertices (routers) and edges (links) respectively. Since unsolicited
* updates are exchanged every 30 seconds, the convergence might require a long time.
*
* For the RIP protocol, the exact convergence time is shorter, thanks to the
* use of triggered updates, which are sent when a route changes.
* Even with triggered updates, the convergence is in the order of magnitude of
* O(|V|*|E|) * 5 seconds, which is still quite long for complex topologies.
*
* \todo: Add routing table compression (CIDR). The most evident result: without
* it a router will announce to be the default router *and* more RTEs, which is silly.
*/

/**
* \ingroup rip
* \brief Rip Routing Table Entry
*/
class RipRoutingTableEntry : public Ipv4RoutingTableEntry
{
public:

	/**
	* Route status
	*/
	enum Status_e {
	RIP_VALID,
	RIP_INVALID,
	};

	RipRoutingTableEntry (void);

	/**
	* \brief Constructor
	* \param network network address
	* \param networkPrefix network prefix
	* \param nextHop next hop address to route the packet
	* \param interface interface index
	*/
	RipRoutingTableEntry (Ipv4Address network, Ipv4Mask networkPrefix, Ipv4Address nextHop, uint32_t interface);

	/**
	* \brief Constructor
	* \param network network address
	* \param networkPrefix network prefix
	* \param interface interface index
	*/
	RipRoutingTableEntry (Ipv4Address network, Ipv4Mask networkPrefix, uint32_t interface);

	virtual ~RipRoutingTableEntry ();

	/**
	* \brief Set the route tag
	* \param routeTag the route tag
	*/
	void SetRouteTag (uint16_t routeTag);

	/**
	* \brief Get the route tag
	* \returns the route tag
	*/
	uint16_t GetRouteTag (void) const;

	/**
	* \brief Set the route metric
	* \param routeMetric the route metric
	*/
	void SetRouteMetric (uint8_t routeMetric);

	/**
	* \brief Get the route metric
	* \returns the route metric
	*/
	uint8_t GetRouteMetric (void) const;

	/**
	* \brief Set the route reference distance
	* \param routeMetric the route reference distance
	*/
	void SetReferenceDistance (uint8_t refDist);

	/**
	* \brief Get the route reference distance
	* \returns the route reference distance
	*/
	uint8_t GetReferenceDistance (void) const;

	/**
	* \brief Set the route status
	* \param status the route status
	*/
	void SetRouteStatus (Status_e status);

	/**
	* \brief Get the route status
	* \returns the route status
	*/
	Status_e GetRouteStatus (void) const;

	/**
	* \brief Set the route as changed
	*
	* The changed routes are scheduled for a Triggered Update.
	* After a Triggered Update, all the changed flags are cleared
	* from the routing table.
	*
	* \param changed true if route is changed
	*/
	void SetRouteChanged (bool changed);

	/**
	* \brief Get the route changed status
	*
	* \returns true if route is changed
	*/
	bool IsRouteChanged (void) const;

private:
	uint16_t m_tag; //!< route tag
	uint8_t m_metric; //!< route metric
	uint8_t m_referenceDistance; //!< route reference distance
	Status_e m_status; //!< route status
	bool m_changed; //!< route has been updated
};

/**
* \brief Stream insertion operator.
*
* \param os the reference to the output stream
* \param route the Ipv4 routing table entry
* \returns the reference to the output stream
*/
std::ostream& operator<< (std::ostream& os, RipRoutingTableEntry const& route);



class RipDistanceTableEntry
{
public:

	RipDistanceTableEntry (void);

	RipDistanceTableEntry (Ipv4Address requestNetwork, Ipv4Mask requestNetworkMask, Ipv4Address gateway, uint32_t interface, uint32_t distance, uint8_t refDist);

	virtual ~RipDistanceTableEntry ();

	void Print (std::ostream& os);

	void SetRouteInterface(uint32_t interface);

	uint32_t GetRouteInterface (void) const;

	void SetNetwork (Ipv4Address requestNetwork);

	Ipv4Address GetNetwork (void) const;

	void SetNetworkMask (Ipv4Mask requestNetworkMask);

	Ipv4Mask GetNetworkMask (void) const;

	void SetGateway (Ipv4Address gateway);

	Ipv4Address GetGateway (void) const;

	void SetRouteDistance (uint32_t dist);

	uint32_t GetRouteDistance (void) const;

	void SetReferenceDistance (uint8_t refDist);

	uint8_t GetReferenceDistance (void) const;

	void SetValid (bool valid);

	bool GetValid (void) const;
 
private:
	Ipv4Address d_requestNetwork;
	Ipv4Mask d_requestNetworkMask;
	Ipv4Address d_gateway;
	uint32_t d_distance;
	uint8_t d_referenceDistance;
	uint32_t d_interface;
	bool d_valid;
};

class DistanceTable
{
public:

	DistanceTable (void);

	virtual ~DistanceTable();

	void Print(Ptr<OutputStreamWrapper> stream, Time::Unit unit, uint32_t nodeId);

	// Add a new entry to the distance table
	void AddDistanceTableEntry(RipDistanceTableEntry *entry, uint32_t nodeId);

	// Delete an entry in the distance table
	void DeleteDistanceTableEntry(RipDistanceTableEntry *entry);

	// Return the interfaces connected to the that satisfy this route
	RipRoutingTableEntry* GetDistanceTableRoute(Ipv4Address destination);

	// Return the number of valid entries for a specific destination
	uint32_t GetNumValidDTEntries(Ipv4Address destination);

	// Validate entire distance table
	void ValidateDistanceTable(void);

	// Validate entries corresponding to a specific address
	void ValidateDistanceTable(Ipv4Address destination, Ipv4Mask destinationMask);

	/**
	 * \brief Invalidate a DT entry.
	 * \param entry the entry to be removed
	 */
	void InvalidateDTEntry (RipDistanceTableEntry *entry);


private:

	// Set to true to print debug messages
	const bool debug = false;

	// Specify the number of multihop paths
	const uint8_t multiPathNumber = 3;

	// Specify the metric for destination unreachable
	const uint32_t destUnreachable = 100;

	// Container for the distance table entries --- this is the distance table
	std::list<RipDistanceTableEntry *> DistanceTableEntries;

	// Used to hold the count for the number of valid entries corresponding to a particular address
	//std::list<std::pair<Ipv4Address , uint8_t > > validEntries;

	// Iterator for container for the distance table
	typedef std::list<RipDistanceTableEntry *>::iterator DistanceTableEntriesI;

};


class RipPendingRequestEntry
{
public:

	RipPendingRequestEntry (void);

	RipPendingRequestEntry (Ipv4Address requestNetwork, Ipv4Mask requestNetworkMask, uint32_t interface, uint8_t requestingRefDist);

	virtual ~RipPendingRequestEntry ();

	void SetRouteInterface(uint32_t interface);

	uint32_t GetRouteInterface (void) const;

	void SetNetwork (Ipv4Address requestNetwork);

	Ipv4Address GetNetwork (void) const;

	void SetNetworkMask (Ipv4Mask requestNetworkMask);

	Ipv4Mask GetNetworkMask (void) const;

	void SetReferenceDistance (uint8_t refDist);

	uint8_t GetReferenceDistance (void) const;

	void SetForwardTimer(bool event);

	bool GetForwardTimer(void);

private:
	bool p_recentlyForwarded;
	Ipv4Address p_requestNetwork;
	Ipv4Mask p_requestNetworkMask;
	uint8_t p_referenceDistance;
	uint32_t p_interface;
};



/**
* \ingroup rip
*
* \brief RIP Routing Protocol, defined in \RFC{2453}.
*/
class Rip : public Ipv4RoutingProtocol
{
public:
	// /< C-tor
	Rip ();
	virtual ~Rip ();

	/**
	* \brief Get the type ID
	* \return type ID
	*/
	static TypeId GetTypeId (void);

	// From Ipv4RoutingProtocol
	Ptr<Ipv4Route> RouteOutput (Ptr<Packet> p, const Ipv4Header &header, Ptr<NetDevice> oif,
							  Socket::SocketErrno &sockerr);
	bool RouteInput (Ptr<const Packet> p, const Ipv4Header &header, Ptr<const NetDevice> idev,
				   UnicastForwardCallback ucb, MulticastForwardCallback mcb,
				   LocalDeliverCallback lcb, ErrorCallback ecb);
	virtual void NotifyInterfaceUp (uint32_t interface);
	virtual void NotifyInterfaceDown (uint32_t interface);
	virtual void NotifyAddAddress (uint32_t interface, Ipv4InterfaceAddress address);
	virtual void NotifyRemoveAddress (uint32_t interface, Ipv4InterfaceAddress address);
	virtual void SetIpv4 (Ptr<Ipv4> ipv4);
	virtual void PrintRoutingTable (Ptr<OutputStreamWrapper> stream, Time::Unit unit = Time::S) const;

	virtual void PrintDistanceTable(Ptr<OutputStreamWrapper> stream, Time::Unit unit = Time::S);

	/**
	* Split Horizon strategy type. See \RFC{2453}.
	*/
	enum SplitHorizonType_e {
	NO_SPLIT_HORIZON,//!< No Split Horizon
	SPLIT_HORIZON,   //!< Split Horizon
	POISON_REVERSE,  //!< Poison Reverse Split Horizon
	};

	/**
	* Statistics for counting RIP signaling overhead.
	*/
	enum Stat_Event_ID {
	  RECV_REQ,
	  RECV_RESP,
	  SEND_REQ,
	  SEND_RESP
	};

	/**
	* Assign a fixed random variable stream number to the random variables
	* used by this model.  Return the number of streams (possibly zero) that
	* have been assigned.
	*
	* \param stream first stream index to use
	* \return the number of stream indices assigned by this model
	*/
	int64_t AssignStreams (int64_t stream);

	/**
	* \brief Get the set of interface excluded from the protocol
	* \return the set of excluded interfaces
	*/
	std::set<uint32_t> GetInterfaceExclusions () const;

	/**
	* \brief Set the set of interface excluded from the protocol
	* \param exceptions the set of excluded interfaces
	*/
	void SetInterfaceExclusions (std::set<uint32_t> exceptions);

	/**
	* \brief Get the metric for an interface
	* \param interface the interface
	* \returns the interface metric
	*/
	uint8_t GetInterfaceMetric (uint32_t interface) const;

	/**
	* \brief Set the metric for an interface
	* \param interface the interface
	* \param metric the interface metric
	*/
	void SetInterfaceMetric (uint32_t interface, uint8_t metric);

	/**
	* \brief Add a default route to the router through the nextHop located on interface.
	*
	* The default route is usually installed manually, or it is the result of
	* some "other" routing protocol (e.g., BGP).
	*
	* \param nextHop the next hop
	* \param interface the interface
	*/
	void AddDefaultRouteTo (Ipv4Address nextHop, uint32_t interface);



	/**
	* \brief Returns the number of requests received
	*/
	uint32_t GetNumRequestsRecv ();

	/**
	* \brief Returns the size of all of the packets for received requests
	*/
	uint32_t GetRequestsRecvSize ();

	/**
	* \brief Returns the number of requests sent
	*/
	uint32_t GetNumRequestsSent ();

	/**
	* \brief Returns the size of all of the packets for sent requests
	*/
	uint32_t GetRequestsSentSize ();

	/**
	* \brief Returns the number of responses sent
	*/
	uint32_t GetNumResponsesSent ();

	/**
	* \brief Returns the number of responses received
	*/
	uint32_t GetNumResponsesRecv ();

	/**
	* \brief Returns the size of all of the packets for received responses
	*/
	uint32_t GetResponsesRecvSize ();

	/**
	* \brief Returns the size of all of the packets for sent responses
	*/
	uint32_t GetResponsesSentSize ();

	/**
	* \brief These three functions are helper functions when checking for convergence.
	*            They allow the user to delete and copy routing table entries.
	*/
	void DeletePrevRoutingTable(bool verbose);
	void CopyRoutingTableEntry(RipRoutingTableEntry* copyRoute);
	void CopyRoutingTableToPrev(bool verbose);

	/**
	* \brief Compares two RTEs together
	*/
	bool CompareIndividualEntry(RipRoutingTableEntry* route1, RipRoutingTableEntry* route2, bool verbose);
	/**
	* \brief Compares the current routing tables with the value in the previous routing table
	*/
	bool CompareRoutingTables(bool verbose);

	/**
	* \brief Finds when convergence is reached at a node.
	*/
	Time FindTimeOfConvergence(Ptr<OutputStreamWrapper> stream, Time::Unit unit, bool verbose);

	void EnableUseDistanceTables(bool use);

protected:
	/**
	* \brief Dispose this object.
	*/
	virtual void DoDispose ();

	/**
	* Start protocol operation
	*/
	void DoInitialize ();

private:
	/// Container for the network routes - pair RipRoutingTableEntry *, EventId (update event)
	typedef std::list<std::pair <RipRoutingTableEntry *, EventId> > Routes;

	/// Const Iterator for container for the network routes
	typedef std::list<std::pair <RipRoutingTableEntry *, EventId> >::const_iterator RoutesCI;

	/// Iterator for container for the network routes
	typedef std::list<std::pair <RipRoutingTableEntry *, EventId> >::iterator RoutesI;

	/// Container for the pending request entries --- this is the pending request table
	typedef std::list<std::pair <RipPendingRequestEntry *, EventId> > PendingRequests;

	/// Iterator for container for the pending requests
	typedef std::list<std::pair <RipPendingRequestEntry *, EventId> >::iterator PendingRequestsI;


	/**
	* \brief Receive RIP packets.
	*
	* \param socket the socket the packet was received to.
	*/
	void Receive (Ptr<Socket> socket);

	/**
	* \brief Handle RIP requests.
	*
	* \param hdr message header (including RTEs)
	* \param senderAddress sender address
	* \param senderPort sender port
	* \param incomingInterface incoming interface
	* \param hopLimit packet's hop limit
	*/
	void HandleRequests (RipHeader hdr, Ipv4Address senderAddress, uint16_t senderPort, uint32_t incomingInterface, uint8_t hopLimit);

	/**
	* \brief Handle RIP responses.
	*
	* \param hdr message header (including RTEs)
	* \param senderAddress sender address
	* \param incomingInterface incoming interface
	* \param hopLimit packet's hop limit
	* \param type specifies the type of response, false is reply and true is response
	*/
	void HandleResponses (RipHeader hdr, Ipv4Address senderAddress, uint32_t incomingInterface, uint8_t hopLimit, bool type);

	/**
	* \brief Handle SCRIP reply messages .
	*
	* \param hdr message header (including RTEs)
	* \param senderAddress sender address
	* \param incomingInterface incoming interface
	* \param hopLimit packet's hop limit
	*/
	void HandleReplys (RipHeader hdr, Ipv4Address senderAddress, uint32_t incomingInterface, uint8_t hopLimit);

	/**
	* \brief Send SCRIP reply messages .
	*
	* \param rtes routing table entry to reply with
	* \param sendSock socket to send the reply on
	* \param senderAddress sender address to reply to
	*/
	void DoSendRouteReply (RipRte rtes, Ptr<Socket> sendSock, Ipv4Address senderAddress, bool track);

	/**
	* \brief Send periodic hello message.
	*/
	void DoSendHelloMessage (void);

	Ptr<Ipv4Route> DistanceTableLookup (Ipv4Address dest, Ptr<NetDevice> = 0);

	/**
	* \brief Lookup in the forwarding table for destination.
	* \param dest destination address
	* \param interface output interface if any (put 0 otherwise)
	* \return Ipv4Route to route the packet to reach dest address
	*/
	Ptr<Ipv4Route> Lookup (Ipv4Address dest, Ptr<NetDevice> = 0);

	/**
	* Receive and process unicast packet
	* \param socket socket where packet is arrived
	*/
	void RecvUnicastRip (Ptr<Socket> socket);
	/**
	* Receive and process multicast packet
	* \param socket socket where packet is arrived
	*/
	void RecvMulticastRip (Ptr<Socket> socket);

	/**
	* \brief Add route to network.
	* \param network network address
	* \param networkPrefix network prefix
	* \param nextHop next hop address to route the packet.
	* \param interface interface index
	*/
	void AddNetworkRouteTo (Ipv4Address network, Ipv4Mask networkPrefix, Ipv4Address nextHop, uint32_t interface);

	/**
	* \brief Add route to network.
	* \param network network address
	* \param networkPrefix network prefix
	* \param interface interface index
	*/
	void AddNetworkRouteTo (Ipv4Address network, Ipv4Mask networkPrefix, uint32_t interface);

	/**
	* \brief Send Routing Updates on all interfaces.
	* \param periodic true for periodic update, else triggered.
	*/
	void DoSendRouteUpdate (bool periodic);

	/**
	* \brief Send Full Table Routing Request on all interfaces.
	*/
	void SendFullTableRequest ();

	/**
	* \brief Send Routing Request for individual RTE on all interfaces.
	*/
	void SendIndividualRouteRequest (RipRte rtes, uint32_t iFace);

	/**
	* \brief Send Triggered Routing Updates on all interfaces.
	*/
	void SendTriggeredRouteUpdate ();

	/**
	* \brief Send Unsolicited Routing Updates on all interfaces.
	*/
	void SendUnsolicitedRouteUpdate (void);

	// Control the timer for the PRT
	void AllowPRTSend (RipPendingRequestEntry *route);

	/**
	* \brief Invalidate a route.
	* \param route the route to be removed
	*/
	void InvalidateRoute (RipRoutingTableEntry *route);

	/**
	* \brief Delete a route.
	* \param route the route to be removed
	*/
	void DeleteRoute (RipRoutingTableEntry *route);

	/**
	* \brief Delete a prt entry.
	* \param entry the entry to be removed
	*/
	void DeletePRTEntry (RipPendingRequestEntry *entry);

	/**
	* \brief Select which stat to update 
	*/
	void UpdateStat (Stat_Event_ID event, uint32_t msg_size = 0);

	/**
	* \brief Counts the number of requests sent and the size of these packets
	*/
	void UpdateStatOnSendRequest (uint32_t msg_size);

	/**
	* \brief Counts the number of requests received and the size of these packets
	*/
	void UpdateStatOnReceiveRequest (uint32_t msg_size);

	/**
	* \brief Counts the number of responses sent and the size of these packets
	*/
	void UpdateStatOnSendResponse (uint32_t msg_size);

	/**
	* \brief Counts the number of responses received and the size of these packets 
	*/
	void UpdateStatOnReceiveResponse (uint32_t msg_size);

	/**
	* Regulate the rate at which requests are forwarded.
	*/
	void SetForwardRequestTracker(bool val);
	bool GetForwardRequestTracker(void);


	// Values required to check for convergence
	Routes prev_routes;
	Time convergenceStartTime;
	int16_t convergenceTimePassed;
	bool firstConvergenceCheck = false;
	#define QUIET_TIME (Seconds(300.0))

	// Define this value to control how often requests are forwarded.
	bool forwardRequest;
	Time m_forwardingDelay;

	// Define the pending request table
	PendingRequests m_pendingRequestTable;

	// Define the distance table
	bool m_useDT;
	DistanceTable m_distanceTable;

	Routes m_routes; //!<  the forwarding table for network.
	Ptr<Ipv4> m_ipv4; //!< IPv4 reference
	Time m_startupDelay; //!< Random delay before protocol startup.
	Time m_minTriggeredUpdateDelay; //!< Min cooldown delay after a Triggered Update.
	Time m_maxTriggeredUpdateDelay; //!< Max cooldown delay after a Triggered Update.
	Time m_unsolicitedUpdate; //!< time between two Unsolicited Routing Updates
	Time m_timeoutDelay; //!< Delay before invalidating a route
	Time m_garbageCollectionDelay; //!< Delay before deleting an INVALID route

	// note: we can not trust the result of socket->GetBoundNetDevice ()->GetIfIndex ();
	// it is dependent on the interface initialization (i.e., if the loopback is already up).
	/// Socket list type
	typedef std::map< Ptr<Socket>, uint32_t> SocketList;
	/// Socket list type iterator
	typedef std::map<Ptr<Socket>, uint32_t>::iterator SocketListI;
	/// Socket list type const iterator
	typedef std::map<Ptr<Socket>, uint32_t>::const_iterator SocketListCI;

	SocketList m_sendSocketList; //!< list of sockets for sending (socket, interface index)
	Ptr<Socket> m_recvSocket; //!< receive socket

	EventId m_nextUnsolicitedUpdate; //!< Next Unsolicited Update event
	EventId m_nextTriggeredUpdate; //!< Next Triggered Update event

	Ptr<UniformRandomVariable> m_rng; //!< Rng stream.

	std::set<uint32_t> m_interfaceExclusions; //!< Set of excluded interfaces
	std::map<uint32_t, uint8_t> m_interfaceMetrics; //!< Map of interface metrics

	SplitHorizonType_e m_splitHorizonStrategy; //!< Split Horizon strategy

	bool m_initialized; //!< flag to allow socket's late-creation.
	uint32_t m_linkDown; //!< Link down value.

	// Statistics Counters
	uint32_t m_requestSentNumber = 0;
	uint32_t m_requestSentSize = 0;
	uint32_t m_requestRecvNumber = 0;
	uint32_t m_requestRecvSize = 0;

	uint32_t m_responseSentNumber = 0;
	uint32_t m_responseSentSize = 0;
	uint32_t m_responseRecvNumber = 0;
	uint32_t m_responseRecvSize = 0;
};

} // namespace ns3
#endif /* RIP_H */

